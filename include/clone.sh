#!/bin/bash

flecs_version="v3.2.0"
raylib_version="4.5.0"
raygui_version="3.2"
cjson_version="v1.7.15"

echo Creating directories
mkdir tmp
mkdir tmp/flecs
mkdir tmp/raylib
mkdir tmp/raygui
mkdir tmp/cjson
mkdir flecs
mkdir raylib
mkdir raygui
mkdir cjson

echo Downloading repos - flecs
git clone --depth 1 --branch $flecs_version https://github.com/sandermertens/flecs tmp/flecs

echo Downloading repos - raylib
git clone --depth 1 --branch $raylib_version https://github.com/raysan5/raylib tmp/raylib

echo Downloading repos - raygui
git clone --depth 1 --branch $raygui_version https://github.com/raysan5/raygui tmp/raygui

echo Downloading repos - cJSON
git clone --depth 1 --branch $cjson_version https://github.com/DaveGamble/cJSON tmp/cjson

echo Moving resources
cp -r tmp/flecs/flecs.* flecs
cp -r tmp/raylib/src/* raylib
cp -r tmp/raygui/src/raygui.h raygui
cp -r tmp/cjson/cJSON.* cjson

echo Cleaning resources
rm -R tmp

echo End
