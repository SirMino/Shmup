#include "enemy.h"

#include "../admins/entity.h"
#include "../structs/datatypes.h"
#include "../components/enemy.h"
#include "../components/speed.h"
#include "../components/vectors.h"

enum do_until { time, spatial };
struct limit {
    enum do_until do_until_t;
    f32 actual_limit;
};

struct package {
    Position *position;
    Velocity *velocity;
    Speed speed;
};

u16 state_first(void);
u16 state_second(void);
u16 state_third(void);

u16 (* states[])(void) = { state_first, state_second, state_third};
enum state_codes { first, second, third };
enum return_codes { ok, fail, repeat };

struct transition {
    enum state_codes source_state;
    enum return_codes return_code;
    enum state_codes dest_state;
};

u16 state_first() {
    return 0;
}

u16 state_second() {
    return 0;
}

u16 state_third() {
    return 0;
}

typedef void (*State)(f32, struct package);
State state;

void state_go_down(f32 delta, struct package pack); 
void state_go_right(f32 delta, struct package pack); 
void state_go_left(f32 delta, struct package pack) {
    if (pack.position->x > 10)
        pack.velocity->x = (0.6) * -1;
    else
     state = state_go_right;
}

void state_go_right(f32 delta, struct package pack)  {
    if (pack.position->x < 210)
        pack.velocity->x = pack.speed.max_value * delta;
    else
     state = state_go_left;
}

void state_go_down(f32 delta, struct package pack) {
    
}

void enemy_engine_update(ecs_iter_t *it) {

    // todo
    ShmEnemy *enemy = ecs_field(it, ShmEnemy, 1);
    Position *pos = ecs_field(it, Position, 2);
    Velocity *vel = ecs_field(it, Velocity, 3);
    Speed *speed = ecs_field(it, Speed, 4);

    if (state == NULL) {
        TraceLog(LOG_DEBUG, "init state");
        state = state_go_left;
    }

    for (u8 i = 0; i < it->count; ++i) {

        struct package package = { &pos[i], &vel[i], speed[i] };

        switch (enemy[i].type) {
            // ideally this should be handled by Lua coroutines, but let's stick to c for now
            case SHMUP_ENEMY_POPCORN1:
                if (vel[i].x == 0.0f && vel[i].y == 0.0f) {
                    vel[i].x = 1.0;
                    //vel[i].y = 1.0;
                }
                
                // VectorI2 direction = (VectorI2) { input->x_axis, input->y_axis };

                if (pos[i].x <= 4) {
                    vel[i].x = 1.0f;
                }

                if (pos[i].x >= 210)
                    vel[i].x = -1.0f;

            break;
            
            case SHMUP_ENEMY_POPCORN2:
                // movement
                state(it->delta_time, package);
                
                // shooting
                if (enemy[i].shoot_cooldown <= 0.0f) {
                    fire_bullet(it->world, (Vector2) { pos[i].x, pos[i].y + 5 }, 300.0, (Direction) {0,1}, 1);
                    enemy[i].shoot_cooldown = 2;
                } else {
                    enemy[i].shoot_cooldown -= it->delta_time;
                }

            break;
            
            default:

            break;
        }

    }
}
