#include "../../include/raylib/raylib.h"

#include "../admins/entity.h"
#include "../components/input.h"
#include "../components/state.h"

#include "input.h"

void input_engine_update(ecs_iter_t *it)
{
    //State * state = ecs_singleton_get_mut(it->world, State);
    Input * input = ecs_singleton_get_mut(it->world, Input);
    input->debug = IsKeyPressed(KEY_D);
    input->esc = IsKeyPressed(KEY_ESCAPE);
    input->up = IsKeyPressed(KEY_UP);
    input->down = IsKeyPressed(KEY_DOWN);
    input->left = IsKeyDown(KEY_LEFT);
    input->right = IsKeyDown(KEY_RIGHT);
    input->z = IsKeyDown(KEY_Z);
    input->x = IsKeyDown(KEY_X);
    input->c = IsKeyDown(KEY_C);
    input->enter = IsKeyPressed(KEY_ENTER) || IsKeyPressed(KEY_KP_ENTER);

    // axis
    if (IsKeyDown(KEY_LEFT) || IsKeyDown(KEY_RIGHT))
    {
        if (IsKeyDown(KEY_LEFT))
            input->x_axis = -1;
        else
            input->x_axis = 1;
    } else {
        input->x_axis = 0;
    }

    if (IsKeyDown(KEY_UP) || IsKeyDown(KEY_DOWN))
    {
        if (IsKeyDown(KEY_UP))
            input->y_axis = -1;
        else
            input->y_axis = 1;
    } else {
        input->y_axis = 0;
    }

/*     if (input->debug)
    {
        state->debug = !state->debug;
    } */
}