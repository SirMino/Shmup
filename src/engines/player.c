#include "player.h"


#include "../admins/entity.h"
#include "../admins/view.h"
#include "../admins/animation.h"

#include "../../include/raylib/raymath.h"

#include "../components/input.h"
#include "../components/vectors.h"
#include "../components/speed.h"
#include "../components/player.h"
#include "../components/weapon.h"
#include "../components/animation.h"
#include "../components/view.h"
#include "../components/tags.h"

static f32 timer = 0.0;

void player_engine_update(ecs_iter_t *it)
{
    const Input * input = ecs_singleton_get(it->world, Input);

    Player *player = ecs_field(it, Player, 1);
    Position *pos = ecs_field(it, Position, 2);
    Velocity *vel = ecs_field(it, Velocity, 3);
    Speed *spd = ecs_field(it, Speed, 4);
    Weapon *weapon = ecs_field(it, Weapon, 5); 
    Animation *anim = ecs_field(it, Animation, 6);   

    for (u8 i = 0; i < it->count; i++)
    {
        vel[i] = Vector2Zero();
        VectorI2 direction = (VectorI2) { input->x_axis, input->y_axis };

        if (player->input_active)
        {
            vel[i].x = input->x_axis;
            vel[i].y = input->y_axis;

            Vector2 _speeded = Vector2Scale(Vector2Normalize(vel[i]), spd[i].max_value);
            vel[i] = Vector2Scale(_speeded, it->delta_time);

            if (vel[i].x != 0)
            {
                if (anim[i].id != SHMUP_ANIMATIONS_SHIP_STRAFE)
                    anim[i] = *get_animation(SHMUP_ANIMATIONS_SHIP_STRAFE);

                anim[i].flip_x = vel[i].x > 0;
            }
            
            if (vel[i].x == 0 && anim[i].id != SHMUP_ANIMATIONS_SHIP_IDLE)
                anim[i] = *get_animation(SHMUP_ANIMATIONS_SHIP_IDLE);

            // fire shoot
            if (input->z)
            {
                if (timer <= 0)
                {
                    fire_bullet(it->world, pos[i], weapon[i].speed, (Direction) {0,-1}, weapon[i].bullet_id);
                    timer = weapon[i].fire_interval;
                } else {
                    timer -= it->delta_time;
                }
            } else {
                if (timer > 0.0)
                    timer = 0.0;
            }
        }

        // if not y input follow camera
        /*
        if (vel[i].y == 0.0f)
        {
            View * _view = get_current_view();
            //Vector2 wpos = GetScreenToWorld2D(pos[i], _view->camera);
            Vector2 screen_pos = GetWorldToScreen2D(pos[i], _view->camera);
            Vector2 wpos = GetScreenToWorld2D(screen_pos, _view->camera);


            //level_t *level = get_current_level();
            //Vector2 diff = Vector2Subtract( (Vector2) {pos[i].coords.x, pos[i].coords.y}, (Vector2) {level->halt_point.x, level->halt_point.y});
            //float diff_length = Vector2Length(diff);
            //TraceLog(LOG_DEBUG, "length between pos and halt %.2f", diff_length);
            ecs_iter_t iter = ecs_filter_iter(it->world, filter_foll);
            while (ecs_filter_next(&iter)) {
                Velocity *v = ecs_field(&iter, Velocity, 2);        
                for (int i = 0; i < iter.count; i++) {
                    vel[i].y = v[i].y;
                    break;
                }
            }
        }
        */
    }
}
