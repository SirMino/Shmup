#include "audio.h"

#include "../structs/datatypes.h"
#include "../components/sound.h"

void audio_engine_udpate(ecs_iter_t *it)
{
    ShmSound *sound = ecs_field(it, ShmSound, 1);
    
    for (u16 i = 0; i < it->count; ++i)
    {
        if (!sound[i].played)
        {
            Sound _sound = *get_sound(sound[i].type); 
            SetSoundPan(_sound, sound[i].pan);
            // PlaySoundMulti(_sound);
            PlaySound(_sound);
            sound[i].played = true;
        }
    }
}
