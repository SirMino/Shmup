#include "bullet.h"

#include "../structs/datatypes.h"
#include "../components/vectors.h"
#include "../components/bullet.h"
#include "../components/speed.h"

#include "../../include/raylib/raymath.h"

void bullet_engine_update(ecs_iter_t *it)
{
    Bullet *bullet = ecs_field(it, Bullet, 1);
    Position *pos = ecs_field(it, Position, 2);
    Velocity *vel = ecs_field(it, Velocity, 3);
    Direction *dir = ecs_field(it, Direction, 4);
    Speed *spd = ecs_field(it, Speed, 5);

    for (u8 i = 0; i < it->count; i++)
    {
        if (bullet[i].lifespan > 0)
        {
            vel[i].x = (dir[i].x * spd[i].max_value) * it->delta_time;
            vel[i].y = (dir[i].y * spd[i].max_value) * it->delta_time;
            //vel[i].y = -spd[i].max_value * it->delta_time;
            bullet[i].lifespan -= it->delta_time;
        } else {
            // delete bullet
            ecs_delete(it->world, it->entities[i]);
        }
    }
}
