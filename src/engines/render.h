#ifndef RENDER_ENGINE_H
#define RENDER_ENGINE_H

#include "../../include/flecs/flecs.h"

void render_engine_update(ecs_iter_t *it);
void render_sprites(ecs_iter_t *it);
void render_shadows(ecs_iter_t *it);
void render_texts(ecs_iter_t *it);
void render_view(ecs_iter_t *it);
void render_screen(ecs_iter_t *it);
void render_ui(ecs_iter_t *it);
void render_debug(ecs_iter_t *it);

#endif