#include <math.h>


#include "../../include/raylib/raylib.h"
#include "../../include/raylib/raymath.h"

#define RAYGUI_IMPLEMENTATION
#include "../../include/raygui/raygui.h"
#undef RAYGUI_IMPLEMENTATION

#include "../globals.h"

#include "../admins/configuration.h"
#include "../admins/texture.h"
#include "../admins/level.h"
#include "../admins/view.h"
#include "../admins/filters.h"
#include "../components/sprite.h"
#include "../components/animation.h"
#include "../components/vectors.h"
#include "../components/shapes.h"
#include "../components/screen.h"
#include "../components/text.h"
#include "../components/player.h"
#include "../components/state.h"
#include "../components/ui.h"
#include "../components/input.h"
#include "../components/speed.h"
#include "../components/tags.h"

#include "render.h"

#define HIGHLIGHT_OFFSET 20

static inline int _lerp_color(int value)
{
    if (value + HIGHLIGHT_OFFSET > 255)
        return 255;

    return value + HIGHLIGHT_OFFSET;
}

static inline Color _get_highlight_color(Color color) {
    Color _highlight_color = (Color) {0, 0, 0, 255 };
    _highlight_color.r = _lerp_color(color.r);
    _highlight_color.g = _lerp_color(color.g);
    _highlight_color.b = _lerp_color(color.b);
    return _highlight_color;
}

void render_engine_update(ecs_iter_t *it)
{ }

void render_sprites(ecs_iter_t *it)
{
    RenderTexture2D *_viewport = get_viewport();
    View * _view = get_current_view();
    Sprite *sprite = ecs_field(it, Sprite, 1);
    Position *pos = ecs_field(it, Position, 2);

    //TraceLog(LOG_DEBUG, TextFormat("Sprites to render %d", it->count));
    BeginTextureMode(*_viewport);

    if (_view->has_camera)
        BeginMode2D(_view->camera);


    for (u8 i = 0; i < it->count; i++)
    {
        i16 pos_y, pos_x;
        //TraceLog(LOG_DEBUG, TextFormat("Render sprite %d", i));
        pos_x = sprite[i].offset.x != 0 ? pos[i].x + sprite[i].offset.x : pos[i].x;
        pos_y = sprite[i].offset.y != 0 ? pos[i].y + sprite[i].offset.y : pos[i].y;
        //DrawTexture(*sprite[i].texture, pos_x, pos_y, WHITE);
        DrawTextureRec(*sprite[i].texture, sprite[i].frame, (Vector2) {pos_x,pos_y}, WHITE);
        // TODO shadow parameter to json?
        //if (sprite[i].has_shadow)
        //    DrawTextureRec(*sprite[i].texture, sprite[i].frame, (Vector2) {pos_x+32,pos_y+32}, Fade(BLACK, 0.3));
    }

    if (_view->has_camera)
        EndMode2D();
        
    EndTextureMode();
}


void render_shadows(ecs_iter_t *it)
{
    RenderTexture2D *_viewport = get_viewport();
    View * _view = get_current_view();
    Sprite *sprite = ecs_field(it, Sprite, 1);
    Position *pos = ecs_field(it, Position, 2);

    BeginTextureMode(*_viewport);

    if (_view->has_camera)
        BeginMode2D(_view->camera);


    for (u8 i = 0; i < it->count; i++)
    {
        i16 pos_y, pos_x;
        pos_x = sprite[i].offset.x != 0 ? pos[i].x + sprite[i].offset.x : pos[i].x;
        pos_y = sprite[i].offset.y != 0 ? pos[i].y + sprite[i].offset.y : pos[i].y;
        // TODO shadow parameter to json?
        if (sprite[i].has_shadow)
            DrawTextureRec(*sprite[i].texture, sprite[i].frame, (Vector2) {pos_x+32,pos_y+32}, Fade(BLACK, 0.3));
    }

    if (_view->has_camera)
        EndMode2D();
        
    EndTextureMode();
}

void camera_update(ecs_iter_t *it)
{
    View * _view = get_current_view();
    Position *pos = ecs_field(it, Position, 2);
    
    static f32 minSpeed = 1;
    static f32 fractionSpeed = 0.8f;
    
    for (int i = 0; i < it->count; i++)
    {
        Vector2 follower_pos = (Vector2) {pos[i].x, pos[i].y};
        Vector2 diff = Vector2Subtract(follower_pos, _view->camera.target);
        f32 length = Clamp(Vector2Length(diff), 1.0, 10.0);
        f32 speed = fmaxf(fractionSpeed*length, minSpeed);

        _view->camera.target = Vector2Add(_view->camera.target, Vector2Scale(diff, speed*it->delta_time/length));
 	    if (_view->camera.target.x < _view->camera.offset.x)
			_view->camera.target.x = _view->camera.offset.x;

		if (_view->camera.target.x > 512 - _view->camera.offset.x)
			_view->camera.target.x = 512 - _view->camera.offset.x;

		if (_view->camera.target.y < _view->camera.offset.y)
        {
            //TraceLog(LOG_DEBUG, "target y < offset y");
			_view->camera.target.y = _view->camera.offset.y;
        }

		if (_view->camera.target.y > 1024.f - _view->camera.offset.y)
        {
			_view->camera.target.y = 1024.f - _view->camera.offset.y;
        }
    }
}

void render_texts(ecs_iter_t *it)
{
    RenderTexture2D *_viewport = get_viewport();
    //Screen * screen = get_current_screen();
    RenderText *txt = ecs_field(it, RenderText, 1);
    Position *pos = ecs_field(it, Position, 2);

    game_config_t *config = get_configuration();

    BeginTextureMode(*_viewport);

        for (u8 i = 0; i < it->count; i++)
        {
            u32 _txt_width = 0;

            // Horizontal align
            if (txt[i].h_align != NONE)
            {
                switch (txt[i].h_align) {
                    case LEFT:
                    break;
                    case CENTER:
                        // _txt_width = MeasureText(txt[i].text, txt[i].selected ? txt[i].font_size + HIGHLIGHT_OFFSET : txt[i].font_size);
                        _txt_width = MeasureTextEx(config->font, txt[i].text, txt[i].selected ? txt[i].font_size + HIGHLIGHT_OFFSET : txt[i].font_size, 1.5).x;
                        pos[i].x = (config->native_resolution.width / 2) - _txt_width / 2;
                    break;
                    case RIGHT:
                    break;
                }
            }

            // Vertical align
            if (txt[i].v_align != NONE)
            {
                switch (txt[i].v_align) {
                    case LEFT:
                    break;
                    case CENTER:
                        pos[i].y = (config->native_resolution.height / 2) - (txt[i].selected ? txt[i].font_size + HIGHLIGHT_OFFSET : txt[i].font_size) / 2;
                    break;
                    case RIGHT:
                    break;
                }
            }

            Color _color = txt[i].color;
            if (txt[i].highlight)
            {
                Vector2 _mouse = GetMousePosition();
                Rectangle txtBound = (Rectangle) { pos[i].x, pos[i].y, MeasureText(txt[i].text, txt[i].font_size), txt[i].font_size };
                if (CheckCollisionPointRec(_mouse, txtBound))
                    _color = _get_highlight_color(_color);
            }

            DrawTextEx(
                get_configuration()->font, 
                txt[i].text, 
                (Vector2) {pos[i].x, pos[i].y}, 
                txt[i].selected ? txt[i].font_size + HIGHLIGHT_OFFSET : txt[i].font_size, 
                1.5, 
                _color);
        }

    EndTextureMode();
}

static u8 menu_index = 0;
void render_ui(ecs_iter_t *it)
{
    game_config_t *config = get_configuration();
    const Input * input = ecs_singleton_get(it->world, Input);
    RenderTexture2D *_viewport = get_viewport();

    ButtonText *btn = ecs_field(it, ButtonText, 2);
    Position *pos = ecs_field(it, Position, 3);

    if (input->down)
        menu_index++;
    if (input->up)
        menu_index--;

    menu_index = (int) Clamp(menu_index, 0, it->count - 1);

    if (input->enter)
        btn[menu_index].callback(it->world);

    BeginTextureMode(*_viewport);
    for (u8 i = 0; i < it->count; i++)
    {
        if (i == menu_index)
        {
            //DrawText(btn[i].text, 40, _y, 32, config->palette[2]);
            DrawTextEx(config->font, btn[i].text, pos[i], 34, 1.0, config->palette[1]);
        } else {
            //DrawText(btn[i].text, 40, _y, 32, config->palette[1]);
            DrawTextEx(config->font, btn[i].text, pos[i], 32, 1.0, config->palette[2]);
        }
    }
    EndTextureMode();
}

void render_debug(ecs_iter_t *it)
{    
    const State * state = ecs_singleton_get(it->world, State);
    if (state->debug)
    {
        game_config_t *config = get_configuration();
        View * _view = get_current_view();
        level_t *_level = get_current_level();
        RenderTexture2D *_viewport = get_viewport();

        BeginTextureMode(*_viewport);

        if (_view->has_camera)
            BeginMode2D(_view->camera);

        // Render points
        //DrawTexture(*get_texture(SHMUP_TEXTURE_CROSS), _level->spawn_point.x -8, _level->spawn_point.y -8, WHITE);
        //DrawTexture(*get_texture(SHMUP_TEXTURE_CROSS), _level->halt_point.x -8, _level->halt_point.y -8, WHITE);  

        ecs_iter_t iter = ecs_filter_iter(it->world, get_filter(SHMUP_ECS_FILTER_BB));
        while (ecs_filter_next(&iter)) {
            BoundingBox2D *bb = ecs_field(&iter, BoundingBox2D, 1);
            Position *pos = ecs_field(&iter, Position, 2);
            for (u16 i = 0; i < iter.count; ++i)
            {
                DrawRectangleLinesEx((Rectangle) {pos[i].x + bb[i].x, pos[i].y + bb[i].y, bb[i].width, bb[i].height}, 1.0, GREEN);
            }
        }      

        if (_view->has_camera)
            EndMode2D();

        // DEBUG PANEL ------------------------------------------------------------------------------------------------------------
        const u16 _width = 90;
        u16 _fps = GetFPS();
        u16 _pos_y = 8;
        u16 _pos_x = 240 - _width; //GetScreenWidth() - DEBUG_WIDTH;
        u16 _offset_x = 4;
        u16 _jump_y = 12;
        u16 _font_size = 8;

        //GuiPanel((Rectangle) {_pos_x, 0, _width, 320}, "");
        DrawRectangle(_pos_x, 0, _width, 320, Fade(config->palette[3], 0.9));
        DrawLineEx((Vector2) {_pos_x, 0}, (Vector2) {_pos_x, 320}, 2, config->palette[3]);
        DrawText(TextFormat("FPS %d", _fps), _pos_x + _offset_x, _pos_y, _font_size, config->palette[2]);
        _pos_y += _jump_y;

        iter = ecs_filter_iter(it->world, get_filter(SHMUP_ECS_FILTER_FOLL_POS));
        while (ecs_filter_next(&iter)) {
            Position *p = ecs_field(&iter, Position, 2);      
            for (u16 i = 0; i < iter.count; i++) {
                DrawText("FOLLOWER", _pos_x + _offset_x, _pos_y, _font_size, config->palette[2]);
                _pos_y += _jump_y;
                DrawText(TextFormat("pos {%.1f,%.1f}", p[i].x, p[i].y), _pos_x + _offset_x, _pos_y, _font_size, config->palette[0]);
                _pos_y += _jump_y;
                break;
            }
        }

        iter = ecs_filter_iter(it->world, get_filter(SHMUP_ECS_FILTER_PLAY_POS_VEL));
        while (ecs_filter_next(&iter)) {
            Position *p = ecs_field(&iter, Position, 2);
            Velocity *v = ecs_field(&iter, Velocity, 3);        
            for (int i = 0; i < iter.count; i++) {
                DrawText("PLAYER 1", _pos_x + _offset_x, _pos_y, _font_size, config->palette[2]);
                _pos_y += _jump_y;
                DrawText(TextFormat("pos {%.1f,%.1f}", p[i].x, p[i].y), _pos_x + _offset_x, _pos_y, _font_size, config->palette[0]);
                _pos_y += _jump_y;
                DrawText(TextFormat("vel {%.2f,%.2f}", v[i].x, v[i].y), _pos_x + _offset_x, _pos_y, _font_size, config->palette[0]);
                _pos_y += _jump_y;
                break;
            }
        }
        
        iter = ecs_filter_iter(it->world, get_filter(SHMUP_ECS_FILTER_ROLLINGBG));
        while (ecs_filter_next(&iter)) {
            Position *p = ecs_field(&iter, Position, 2);
            TimeFlow *t = ecs_field(&iter, TimeFlow, 3);
            Sprite *s = ecs_field(&iter, Sprite, 4);

            for (u8 i = 0; i < iter.count; ++i)
            {
                DrawText("ROLLING BG", _pos_x + _offset_x, _pos_y, _font_size, config->palette[2]);
                _pos_y += _jump_y;
                DrawText(TextFormat("frame {%.1f,%.1f}", s[i].frame.x, s[i].frame.y), _pos_x + _offset_x, _pos_y, _font_size, config->palette[0]);
                _pos_y += _jump_y;
                DrawText(TextFormat("time %.1f/%.1f", t[i].time_elapsed, t[i].time_total), _pos_x + _offset_x, _pos_y, _font_size, config->palette[0]);
                _pos_y += _jump_y;
            }
        }

        iter = ecs_filter_iter(it->world, get_filter(SHMUP_ECS_FILTER_BULLETS));
        u32 bullets_number = 0;
        while (ecs_filter_next(&iter))
        {
            for (u32 i = 0; i < iter.count; ++i)
                bullets_number++;
        }
        DrawText("BULLETS", _pos_x + _offset_x, _pos_y, _font_size, config->palette[2]);
        _pos_y += _jump_y;
        DrawText(TextFormat("n. %d", bullets_number), _pos_x + _offset_x, _pos_y, _font_size, config->palette[0]);
        _pos_y += _jump_y;

        if (_view->has_camera)
        {
            DrawText("CAMERA", _pos_x + _offset_x, _pos_y, _font_size, config->palette[2]);
            _pos_y += _jump_y;
            DrawText(TextFormat("tar {%.2f,%.2f}", _view->camera.target.x, _view->camera.target.y), _pos_x + _offset_x, _pos_y, _font_size, config->palette[0]);
            _pos_y += _jump_y;
            DrawText(TextFormat("ofs {%.2f,%.2f}", _view->camera.offset.x, _view->camera.offset.y), _pos_x + _offset_x, _pos_y, _font_size, config->palette[0]);
        }
        
        iter = ecs_filter_iter(it->world, get_filter(SHMUP_ECS_FILTER_ENEMY));
        u32 enemies_number = 0;
        Vector2 mouse_world = Vector2Zero();
        Animation * selected = NULL;
        //if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) {
            Vector2 mouse = GetMousePosition();
            mouse_world = (Vector2) { mouse.x/3, mouse.y/3};
        //}
        while (ecs_filter_next(&iter))
        {
            Position *pos = ecs_field(&iter, Position, 2);
            BoundingBox2D *bb = ecs_field(&iter, BoundingBox2D, 3);
            Animation *ani = ecs_field(&iter, Animation, 5);

            for (u32 i = 0; i < iter.count; ++i) {
                enemies_number++;
                
                if (CheckCollisionPointRec(mouse_world, (Rectangle) {pos[i].x + bb[i].x, pos[i].y + bb[i].y, bb[i].width, bb[i].height})) {
                    selected = &ani[i];
                }
            }

        }
        DrawText("ENEMIES", _pos_x + _offset_x, _pos_y, _font_size, config->palette[2]);
        _pos_y += _jump_y;
        DrawText(TextFormat("n. %d", enemies_number), _pos_x + _offset_x, _pos_y, _font_size, config->palette[0]);
        _pos_y += _jump_y;
        if (selected != NULL) {
            DrawText(TextFormat("frame %d/%d", selected->current_frame, selected->frames_number), _pos_x + _offset_x, _pos_y, _font_size, config->palette[0]);
            _pos_y += _jump_y;
            DrawText(TextFormat("elapsed %.2f", selected->frame_timer), _pos_x + _offset_x, _pos_y, _font_size, config->palette[0]);
            _pos_y += _jump_y;
        }

/*         if (GuiButton((Rectangle) {_pos_x, _pos_y, 60, 12}, "click me"))
        {
            TraceLog(LOG_DEBUG, "btn");
        }
        _pos_y += _jump_y * 2;

        bool checked = false;
        checked = GuiCheckBox((Rectangle) {_pos_x, _pos_y, 12, 12}, "check", checked);
        if (checked)
        {
            TraceLog(LOG_DEBUG, "checked");
        } */
        // DEBUG PANEL ------------------------------------------------------------------------------------------------------------
    }
    EndTextureMode();
}

void render_view(ecs_iter_t *it)
{
    View * _view = get_current_view();
    game_config_t *_config = get_configuration();
    RenderTexture2D *_viewport = get_viewport();

    //BeginDrawing();
    BeginTextureMode(*_viewport);

    //if (_view->has_camera)
    //    BeginMode2D({NULL});

    if (_view->background != NULL)
    {
        DrawTexture(*_view->background, 0, 0, WHITE);
    }
    else
    {
        ClearBackground(_view->background_color);    
    }

/*      DrawTexturePro(
        _viewport->texture, 
        (Rectangle) { 0, 0, _config->native_resolution.width, _config->native_resolution.height }, 
        (Rectangle) {0, 0, _config->render_resolution.width, _config->render_resolution.height}, 
        Vector2Zero(), 
        0.0f, 
        RAYWHITE); */
    EndTextureMode();
    //EndDrawing();
}

void render_screen(ecs_iter_t *it)
{
    //TraceLog(LOG_DEBUG, "render_screen");
    GameScreen *_screen = ecs_field(it, GameScreen, 1);
    View * _view = get_current_view();
    RenderTexture2D *_viewport = get_viewport();

    BeginDrawing();
    ClearBackground(_view->background_color);
    Rectangle _source = _screen->source;
    _source.height *= -1;
    DrawTexturePro(
        _viewport->texture, 
        //(Rectangle) {_screen->source.x, _screen->source.y, _screen->source.width, _screen->source.height *= -1 }, 
        _source,
        _screen->destination, 
        Vector2Zero(), 
        0.0f, 
        WHITE);
    EndDrawing();
}
