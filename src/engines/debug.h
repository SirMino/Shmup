#ifndef DEBUG_ENGINE_H
#define DEBUG_ENGINE_H

#include "../../include/flecs/flecs.h"

void debug_engine_update(ecs_iter_t * it);

#endif