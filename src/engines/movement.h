#ifndef MOVEMENT_ENGINE_H
#define MOVEMENT_ENGINE_H

#include "../../include/flecs/flecs.h"

void movement_engine_update(ecs_iter_t *it);
void follower_update(ecs_iter_t *it);
void rolling_bg_update(ecs_iter_t *it);

#endif