#include "animation.h"

#include "../admins/entity.h"
#include "../components/sprite.h"
#include "../components/animation.h"
#include "../components/vectors.h"
#include "../components/player.h"
#include "../components/tags.h"

void animation_engine_update(ecs_iter_t *it)
{
    Sprite *sprite = ecs_field(it, Sprite, 1);
    Position *pos = ecs_field(it, Position, 2);
    Animation *anim = ecs_field(it, Animation, 3);

    f32 _delta_time = it->delta_time;

    for (u16 i = 0; i < it->count; ++i)
    {
        anim[i].frame_timer += _delta_time;

        if (anim[i].frame_timer >= anim[i].animation_speed)
        {
            if (anim[i].current_frame == anim[i].frames_number - 1) {
                if (anim[i].loop) {
                    anim[i].current_frame = 0;
                } else {
                    // stop or destroy
                    if (anim[i].destroy_after_run) {
                        delete_entity_id(it->world, it->entities[i]);
                        continue;
                    }
                }
            } else {
                anim[i].current_frame = anim[i].current_frame + 1;
            }

            anim[i].frame_timer = 0.0;
            //anim[i].current_frame = anim[i].current_frame == anim[i].frames_number - 1 ? 0 : anim[i].current_frame + 1;
        }

        sprite[i].frame = (Rectangle) {anim[i].frames[anim[i].current_frame].coords.x, anim[i].frames[anim[i].current_frame].coords.y, anim[i].frames[anim[i].current_frame].size.x, anim[i].frames[anim[i].current_frame].size.y};

        if (anim[i].flip_x)
            sprite[i].frame.width *= -1;
    }
}
