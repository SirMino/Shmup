#ifndef INPUT_ENGINE_H
#define INPUT_ENGINE_H

#include "../../include/flecs/flecs.h"

void input_engine_update(ecs_iter_t *it);

#endif