#ifndef SCREEN_ENGINE_H
#define SCREEN_ENGINE_H

#include "../../include/flecs/flecs.h"

void screen_engine_init(ecs_world_t *world);
void screen_engine_render(ecs_iter_t *it);
void screen_engine_update(ecs_iter_t *it);
void screen_engine_end(ecs_world_t *world);

#endif