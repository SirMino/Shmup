#include "collision.h"

#include "../structs/datatypes.h"
#include "../structs/ray2d.h"
#include "../components/vectors.h"
#include "../components/shapes.h"
#include "../components/bullet.h"
#include "../admins/filters.h"
#include "../admins/entity.h"

#include "../../include/raylib/raymath.h"

typedef struct hit {
	bool is_hit;
	f32 time;
	Vector2 pos;
} Hit;

static Hit ray_vs_rect(Vector2 pos, Vector2 magnitude, Rectangle rec) {
	Hit hit = {0};
	Vector2 min = (Vector2) {rec.x, rec.y};
	Vector2 max = (Vector2) {rec.x + rec.width, rec.y + rec.height};

	f32 last_entry = -INFINITY;
	f32 first_exit = INFINITY;

	f32 t1,t2;

	if (magnitude.x != 0) {
		t1 = (min.x - pos.x) / magnitude.x;
		t2 = (max.x - pos.x) / magnitude.x;
		last_entry = fmaxf(last_entry, fminf(t1, t2));
		first_exit = fminf(first_exit, fmaxf(t1, t2));
	} else if (pos.x <= min.x || pos.x >= max.x) {
		return hit;
	}
	if (magnitude.y != 0) {
		t1 = (min.y - pos.y) / magnitude.y;
		t2 = (max.y - pos.y) / magnitude.y;
		last_entry = fmaxf(last_entry, fminf(t1, t2));
		first_exit = fminf(first_exit, fmaxf(t1, t2));
	} else if (pos.y <= min.y || pos.y >= max.y) {
		return hit;
	}

	if (first_exit > last_entry && first_exit > 0 && last_entry < 1) {
	    hit.pos.x = pos.x + magnitude.x * last_entry;
	    hit.pos.y = pos.y + magnitude.y * last_entry;
	    hit.is_hit = true;
	    hit.time = last_entry;
	}

	return hit;
}

void collision_engine_update(ecs_iter_t *it)
{
    Bullet *bullet = ecs_field(it, Bullet, 1);
    BoundingBox2D *bullet_bb = ecs_field(it, BoundingBox2D, 2);
    Position *bullet_pos = ecs_field(it, Position, 3);
    Velocity *bullet_vel = ecs_field(it, Velocity, 4);

    // each bullet
    for (u16 i = 0; i < it->count; ++i)
    {
	//TraceLog(LOG_DEBUG, "bullet %d %d", i, it->entities[i]);
        Rectangle bullet_rect = (Rectangle) { bullet_pos[i].x + bullet_bb[i].x, bullet_pos[i].y + bullet_bb[i].y, bullet_bb[i].width, bullet_bb[i].height };
        Ray2D bullet_ray = (Ray2D) { {bullet_rect.x + (bullet_rect.width / 2),bullet_rect.y},{bullet_vel[i].x, bullet_vel[i].y}};

        // each bb not bullet
	ecs_iter_t enemy_iter = ecs_filter_iter(it->world, get_filter(SHMUP_ECS_FILTER_ENEMY));
        while (ecs_filter_next(&enemy_iter)) {
            Position *enemy_pos = ecs_field(&enemy_iter, Position, 2);
            BoundingBox2D *enemy_bb = ecs_field(&enemy_iter, BoundingBox2D, 3);

            for (u16 j = 0; j < enemy_iter.count; ++j)
            {
		//TraceLog(LOG_DEBUG, "check bullet %d against enemy %d", it->entities[i], enemy_iter.entities[j]);
                Rectangle enemy_rect = (Rectangle) { enemy_pos[j].x + enemy_bb[j].x, enemy_pos[j].y + enemy_bb[j].y, enemy_bb[j].width, enemy_bb[j].height };
                Hit _hit = ray_vs_rect(bullet_ray.origin, bullet_vel[i], enemy_rect);

                if (_hit.is_hit) {
		    if (_hit.time >= 0.0f && _hit.time < 1) {
		    //if (_hit.time < 1) {
                        //TraceLog(LOG_DEBUG, "collision time %.2f", _hit.time);
			delete_entity_id(it->world, enemy_iter.entities[j]);
			delete_entity_id(it->world, it->entities[i]);
			spawn_explosion(it->world, (Vector2) {enemy_rect.x + (enemy_rect.width / 2), enemy_rect.y + (enemy_rect.height / 2) });
		    }
                }
            }
        }
    }
}


