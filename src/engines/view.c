#include "../admins/view.h"

#include "view.h"

void view_engine_init(ecs_world_t *world)
{
    const View * _view = ecs_singleton_get(world, View);
    _view->init_void(world);  
}

void view_engine_update(ecs_iter_t *it)
{
    const View * _view = ecs_singleton_get(it->world, View);
    _view->update_void(it);
}

void view_engine_end(ecs_world_t *world)
{
    const View * _view = ecs_singleton_get(world, View);
    _view->end_void(world);
}
