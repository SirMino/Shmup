#include <stdio.h>
#include "../../include/raylib/raylib.h"

#include "../admins/configuration.h"
#include "../components/text.h"
#include "../components/vectors.h"

#include "../admins/configuration.h"

#include "text.h"

#define HIGHLIGHT_OFFSET 20

static inline int _lerp_color(int value)
{
    if (value + HIGHLIGHT_OFFSET > 255)
        return 255;

    return value + HIGHLIGHT_OFFSET;
}

static inline Color _get_highlight_color(Color color) {
    Color _highlight_color = (Color) {0, 0, 0, 255 };
    _highlight_color.r = _lerp_color(color.r);
    _highlight_color.g = _lerp_color(color.g);
    _highlight_color.b = _lerp_color(color.b);
    return _highlight_color;
}

void text_engine_update(ecs_iter_t *it)
{
    RenderText *txt = ecs_field(it, RenderText, 1);
    Position *pos = ecs_field(it, Position, 2);

    for (int i = 0; i < it->count; i++)
    {
        int _txt_width = 0;

        // Horizontal align
        if (txt[i].h_align != NONE)
        {
            switch (txt[i].h_align) {
                case LEFT:
                break;
                case CENTER:
                    // _txt_width = MeasureText(txt[i].text, txt[i].selected ? txt[i].font_size + HIGHLIGHT_OFFSET : txt[i].font_size);
                    _txt_width = MeasureTextEx(get_configuration()->font, txt[i].text, txt[i].selected ? txt[i].font_size + HIGHLIGHT_OFFSET : txt[i].font_size, 1.5).x;
                    pos[i].x = (GetScreenWidth() / 2) - _txt_width / 2;
                break;
                case RIGHT:
                break;
            }
        }

        // Vertical align
        if (txt[i].v_align != NONE)
        {
            switch (txt[i].v_align) {
                case LEFT:
                break;
                case CENTER:
                    pos[i].y = (GetScreenHeight() / 2) - (txt[i].selected ? txt[i].font_size + HIGHLIGHT_OFFSET : txt[i].font_size) / 2;
                break;
                case RIGHT:
                break;
            }
        }

        Color _color = txt[i].color;
        if (txt[i].highlight)
        {
            Vector2 _mouse = GetMousePosition();
            Rectangle txtBound = (Rectangle) { pos[i].x, pos[i].y, MeasureText(txt[i].text, txt[i].font_size), txt[i].font_size };
            if (CheckCollisionPointRec(_mouse, txtBound))
                _color = _get_highlight_color(_color);
        }

        //DrawText(txt[i].text, pos[i].x, pos[i].y, txt[i].selected ? txt[i].font_size + HIGHLIGHT_OFFSET : txt[i].font_size, _color);
        DrawTextEx(
            get_configuration()->font, 
            txt[i].text, 
            (Vector2) {pos[i].x, pos[i].y}, 
            txt[i].selected ? txt[i].font_size + HIGHLIGHT_OFFSET : txt[i].font_size, 
            1.5, 
            _color);
    }
}