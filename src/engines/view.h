#ifndef VIEW_ENGINE_H
#define VIEW_ENGINE_H

#include "../../include/flecs/flecs.h"

void view_engine_init(ecs_world_t *world);
void view_engine_update(ecs_iter_t *it);
void view_engine_end(ecs_world_t *world);

#endif