#include "../components/state.h"
#include "../components/tags.h"

#include "../../include/raylib/raylib.h"

#include "debug.h"

#define DEBUG_WIDTH 40

void debug_engine_update(ecs_iter_t * it)
{
    const State * state = ecs_singleton_get(it->world, State);
    if (state->debug)
    {
        int _fps = GetFPS();
        int _pos_y = 48;
        int _pox_x = GetScreenWidth() - DEBUG_WIDTH;

        // render raygui debug
        // box
        //GuiPanel((Rectangle) { 100, 0, 60, 1024 }, "");
        //GuiGroupBox((Rectangle) { 100, 0, 60, 1024 }, "");

        // fps number
        //GuiLabel((Rectangle) { _pox_x, _pos_y, DEBUG_WIDTH, 24 }, "FPS");
        //_pos_y += 24;
        //GuiValueBox((Rectangle) {_pox_x, _pos_y, DEBUG_WIDTH, 24}, "", &_fps, 0, 10, false);
        //_pos_y += 34;
    }
/*  
    ecs_filter_t *f = ecs_filter(it->world, {
        .terms = {
            { ecs_id(Drop) }
        }
    });

    if (state->debug)
    {
        int _drops_count = 0;
        ecs_iter_t fit = ecs_filter_iter(it->world, f);
        while (ecs_filter_next(&fit)) {
            for (int i = 0; i < fit.count; i++)
            {
                _drops_count++;
            }
        }

        int _fps = GetFPS();
        int _pos_y = 48;
        int _pox_x = GetScreenWidth() - DEBUG_WIDTH;

        // render raygui debug
        // box
        GuiPanel((Rectangle) { _pox_x, 0, DEBUG_WIDTH, 600 }, "");
        GuiGroupBox((Rectangle) { _pox_x, 0, DEBUG_WIDTH, 600 }, "");

        // fps number
        GuiLabel((Rectangle) { _pox_x, _pos_y, DEBUG_WIDTH, 24 }, "FPS");
        _pos_y += 24;
        GuiValueBox((Rectangle) {_pox_x, _pos_y, DEBUG_WIDTH, 24}, "", &_fps, 0, 10, false);
        _pos_y += 34;

        // drops number
        GuiLabel((Rectangle) { _pox_x, _pos_y, DEBUG_WIDTH, 24 }, "DROPS");
        _pos_y += 24;
        GuiValueBox((Rectangle) {_pox_x, _pos_y, DEBUG_WIDTH, 24}, "", &_drops_count, 0, 10, false);
        _pos_y += 34;

    } */
}