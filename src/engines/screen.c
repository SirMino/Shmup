#include <stdio.h>

#include "../admins/screen.h"

#include "screen.h"

void screen_engine_init(ecs_world_t *world)
{
    printf("screen_engine_init\n");
    Screen * screen = ecs_singleton_get_mut(world, Screen);
    screen->init_void(world);  
}

void screen_engine_update(ecs_iter_t *it)
{
    Screen * screen = ecs_singleton_get_mut(it->world, Screen);
    //Screen *screen = get_current_screen();
    screen->update_void(it);
}

void screen_engine_render(ecs_iter_t *it)
{
    Screen * screen = ecs_singleton_get_mut(it->world, Screen);
    screen->render_void(it);

}

void screen_engine_end(ecs_world_t *world)
{
    printf("screen_engine_end\n");
    Screen * screen = ecs_singleton_get_mut(world, Screen);
    screen->end_void(world);
}
