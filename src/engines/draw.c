#include "draw.h"

#include "../../include/raylib/raylib.h"

#include "../components/sprite.h"
#include "../components/position.h"

void draw_engine_update(ecs_iter_t *it)
{
    Sprite *sprite = ecs_field(it, Sprite, 1);
    Position *pos = ecs_field(it, Position, 2);

    for (int i = 0; i < it->count; i++)
    {
        DrawTexture(*sprite[i].texture, pos[i].x, pos[i].y, WHITE);
        //DrawRectangle(pos[i].x, pos[i].y, dr[i].rect.width, dr[i].rect.height, dr[i].color);
        //DrawText(TextFormat("%f,%f", pos[i].x, pos[i].y), pos[i].x, pos[i].y, 8, GREEN);
    }
}