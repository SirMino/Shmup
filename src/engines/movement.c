#include <stdio.h>
#include "../../include/raylib/raylib.h"
#include "../../include/raylib/raymath.h"

#include "../globals.h"
#include "../admins/level.h"
#include "../admins/configuration.h"
#include "../components/vectors.h"
#include "../components/speed.h"
#include "../components/sprite.h"
#include "../components/player.h"
#include "../components/animation.h"
#include "../components/tags.h"

#include "movement.h"

void movement_engine_update(ecs_iter_t *it)
{
    Position *pos = ecs_field(it, Position, 1);
    Velocity *vel = ecs_field(it, Velocity, 2);
    const game_config_t *config = get_configuration();

    for (u16 i = 0; i < it->count; ++i) {

        // TODO delta time multiply should be done in the movement engine of the entity, here we just should add velocity to position
        // currently follower go through 2 delta time multiply (see follower_update)
        // < actually not sure >
        
        //Vector2 _scaled = Vector2Scale((Vector2) {vel[i].x, vel[i].y}, spd[i].max_value);
        //Vector2 _normalized = Vector2Normalize(_scaled);
        //Vector2 _deltatimed = Vector2Scale(_scaled, it->delta_time);
        
        pos[i] = Vector2Add(pos[i], vel[i]);

        if (ecs_has_id(it->world, it->entities[i], PlayerTag)) {
            ecs_table_t* table = ecs_get_table(it->world, it->entities[i]);
            Animation *anim = ecs_table_get_id(it->world, table, ecs_id(Animation), 0);

            pos[i].x = Clamp(pos[i].x, 0, config->native_resolution.width - anim->frames[anim->current_frame].size.x);
            pos[i].y = Clamp(pos[i].y, 0, config->native_resolution.height - anim->frames[anim->current_frame].size.y);
        }
    }   
}

void follower_update(ecs_iter_t *it)
{
    Position *pos = ecs_field(it, Position, 2);
    TimeFlow *time = ecs_field(it, TimeFlow, 3);
    //Velocity *vel = ecs_field(it, Velocity, 3);
    //Speed *spd = ecs_field(it, Speed, 4);
    level_t *level = get_current_level();

    // linear interpolation
    //Vector2 follower_position = Vector2Add(Vector2Scale(Vector2Subtract(level->halt_point, level->spawn_point), (time_elapsed / time_total)), level->spawn_point);

    for (u8 i = 0; i < it->count; i++) {
        pos[i] = Vector2Add(Vector2Scale(Vector2Subtract(level->halt_point, level->spawn_point), (time[i].time_elapsed / time[i].time_total)), level->spawn_point);
        if (time[i].time_elapsed < time[i].time_total)
        {
            time[i].time_elapsed += it->delta_time;
        }
    }

    /*
    for (uint8_t i = 0; i < it->count; i++) {
        Vector2 diff = Vector2Subtract(pos[i], level->halt_point);
        float len = Vector2Length(diff);
        //TraceLog(LOG_DEBUG, "diff len %.2f", len);
        if (len > 0.5f)
            vel[i].y = -spd[i].max_value * it->delta_time;
        else
            vel[i].y = 0.0f;
    }   
    */
}

void rolling_bg_update(ecs_iter_t *it)
{
    level_t *level = get_current_level();
    RollingBackground *rbg = ecs_field(it, RollingBackground, 1);
    Position *pos = ecs_field(it, Position, 2);
    TimeFlow *time = ecs_field(it, TimeFlow, 3);
    Sprite *sprite = ecs_field(it, Sprite, 4);
    
    for (u8 i = 0; i < it->count; i++) {
        /*
        pos[i] = Vector2Add(
            Vector2Scale(
                Vector2Subtract(
                    level->halt_point, 
                    level->spawn_point), 
                    (time[i].time_elapsed / time[i].time_total)), 
            level->spawn_point
            );
        */
        if (time[i].time_elapsed < time[i].time_total)
            time[i].time_elapsed += it->delta_time;
        else
            time[i].time_elapsed = time[i].time_total;

        sprite[i].frame.x = (rbg[i].frame_end.x - rbg[i].frame_start.x) * (time[i].time_elapsed / time[i].time_total) + rbg[i].frame_start.x;
        sprite[i].frame.y = (rbg[i].frame_end.y - rbg[i].frame_start.y) * (time[i].time_elapsed / time[i].time_total) + rbg[i].frame_start.y;
        //sprite[i].frame.y = (0 - 1040) * (time[i].time_elapsed / time[i].time_total) + 1040;
    }
}
