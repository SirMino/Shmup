#ifndef PLAYER_ENGINE_H
#define PLAYER_ENGINE_H

#include "../../include/flecs/flecs.h"

void player_engine_update(ecs_iter_t *it);
void camera_update(ecs_iter_t *it);

#endif