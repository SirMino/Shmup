#ifndef GAME_VIEW_H
#define GAME_VIEW_H

#include "../../include/flecs/flecs.h"

void game_init(ecs_world_t *world);
void game_update(ecs_iter_t *it);
void game_end(ecs_world_t *world);

#endif
