#include <stdio.h>

#include "../components/text.h"
#include "../components/vectors.h"
#include "../components/input.h"
#include "../components/sprite.h"
#include "../admins/entity.h"
#include "../admins/configuration.h"
#include "../admins/view.h"
#include "../admins/texture.h"
#include "../globals.h"

#include "../../include/raylib/raymath.h"

#include "menu.h"

static enum menu
{
    SHMUP_MENU_GAME_1,
    SHMUP_MENU_GAME_2,
    SHMUP_MENU_SETTINGS,
    SHMUP_MENU_EXIT,
    SHMUP_MENU_ENUM_END
} MenuUI;

static u8 menu_index;

static char* menus[] =
    {
        "Start",
        "Start 2",
        "Settings",
        "Exit"
    };

void game1_click(ecs_world_t *world);
void game2_click(ecs_world_t *world);
void settings_click(ecs_world_t *world);
void exit_click(ecs_world_t *world);

void menu_init(ecs_world_t *world)
{
    View * menu = get_current_view();
    menu->background = get_texture(SHMUP_TEXTURE_MENU_BG);
    //menu_screen->viewport = LoadRenderTexture(get_configuration()->native_resolution.width,get_configuration()->native_resolution.height);
    //SetTextureFilter(menu_screen->viewport.texture, TEXTURE_FILTER_POINT);

    //create_sprite(world, "ResBg", SHMUP_TEXTURE_RES_TEST, (Vector2) {0,0});
    //create_text(world, "txt_shmup", "SHMUP", 32, 10.0, 20.0, false, false, CENTER, NONE, (Color) COLOR_PALETTE_5);
    //create_text(world, "txt_start_help", "push space to", 12, 10.0, 240.0, false, true, CENTER, NONE, (Color) COLOR_PALETTE_4);
    //create_text(world, "txt_start", "-- START --", 24, 10.0, 260.0, false, true, CENTER, NONE, (Color) COLOR_PALETTE_4);    

    // function pointers ftw
    void (*callbacks[SHMUP_MENU_ENUM_END])(ecs_world_t *world) = {game1_click, game2_click, settings_click, exit_click};

    create_menu_ui(world, menus, SHMUP_MENU_ENUM_END, 64, callbacks);
}

void menu_update(ecs_iter_t *it)
{
    const Input * input = ecs_singleton_get(it->world, Input);
    View * menu = get_current_view();
    RenderTexture2D *_viewport = get_viewport();

    if (input->down)
        menu_index = (int)Clamp(menu_index + 1, 0, SHMUP_MENU_ENUM_END);
    if (input->up)
        menu_index = (int)Clamp(menu_index - 1, 0, SHMUP_MENU_ENUM_END);

/*     if (IsKeyPressed(KEY_SPACE))
    {
        TraceLog(LOG_DEBUG, "Let's start %s!!", "S H M U P");
        set_current_view(it->world, SHMUP_VIEW_GAME);
    } */

    
}

void game1_click(ecs_world_t *world) {
    set_current_view(world, SHMUP_VIEW_GAME);
}
void game2_click(ecs_world_t *world) {
    set_current_view(world, SHMUP_VIEW_GAME2);
}
void settings_click(ecs_world_t *world)
{
    TraceLog(LOG_DEBUG, "Settings");
}
void exit_click(ecs_world_t *world) {
    game_config_t *config = get_configuration();
    config->running = false;
}

void run_game(ecs_world_t *world)
{
    set_current_view(world, SHMUP_VIEW_GAME);
}

void menu_end(ecs_world_t *world)
{
    delete_menu_ui(world, menus, SHMUP_MENU_ENUM_END);
}
