#include <stdlib.h>

#include "game2.h"

#include "../admins/entity.h"
#include "../admins/view.h"
#include "../admins/level.h"
#include "../admins/filters.h"

#include "../components/state.h"
#include "../components/input.h"
#include "../components/player.h"

#include "../../include/raylib/raymath.h"

static enum menu
{
    SHMUP_GAME2UI_RESUME,
    SHMUP_GAME2UI_EXIT,
    SHMUP_GAME2UI_ENUM_END,
} InGame2UI;

static char* menus[] =
    {
        "Resume",
        "Exit"
    };

static void _ui_select_resume(ecs_world_t *world);
static void _ui_select_exit(ecs_world_t *world);
static void (*callbacks[SHMUP_GAME2UI_ENUM_END])(ecs_world_t *world) = {_ui_select_resume, _ui_select_exit};

//static ecs_filter_t *filter_player;

static bool menu_visible = false;

void game2_init(ecs_world_t *world)
{
    level_t *level = set_level(SHMUP_LEVELS_2);

    if (level != NULL)
    {
        for (u8 i = 0; i < level->backgrounds_number; ++i)
        {
            if (level->backgrounds[i].order == 0)
            {
                TraceLog(LOG_DEBUG, "creo livello ordine 0");
                create_rolling_bg(world, TextFormat("map%d", i), &level->backgrounds[i], Vector2Zero());
            }
        }

        // enemies
        spawn_enemy(world, SHMUP_ENEMY_POPCORN1, (Vector2) { 20, 10});
        spawn_enemy(world, SHMUP_ENEMY_POPCORN2, (Vector2) { 110, 50});

        create_player(world, "player", (Vector2) {level->spawn_point.x, level->spawn_point.y});
        /*
        u8 enemies = GetRandomValue(4, 10);
        for (u8 i = 0; i < enemies; ++i) {
            f32 _x = GetRandomValue(34, 200);
            f32 _y = GetRandomValue(34, 130);
            spawn_enemy(world, SHMUP_ENEMY_POPCORN1, (Vector2) {_x, _y});
        }
        */
        //spawn_enemy(world, (Vector2) {48, 36});
        //spawn_enemy(world, (Vector2) {120, 48});
        
        //create_sprite(world, "ship2", SHMUP_TEXTURE_SHIP_2, (Vector2) {160,100});
        //create_sprite(world, "test_clouds", SHMUP_TEXTURE_SCROLLING_CLOUDS, (Vector2) {10 ,10});

        for (u8 i = 0; i < level->backgrounds_number; ++i)
        {
            if (level->backgrounds[i].order > 0)
            {
                TraceLog(LOG_DEBUG, "creo livello ordine %d", level->backgrounds[i].order);
                create_rolling_bg(world, TextFormat("map%d", i), &level->backgrounds[i], Vector2Zero());
            }
        }
    }

/*     filter_player = ecs_filter(world, {
        .terms = {
            { ecs_id(Player) }
        }
    }); */
}

f32 time_elasped = 0;
bool spwned = false;
void game2_update(ecs_iter_t *it)
{
    State * state = ecs_singleton_get_mut(it->world, State);
    const Input * input = ecs_singleton_get(it->world, Input);

    if (input->debug)
    {
        state->debug = !state->debug;
    }

    if (input->esc && !menu_visible)
    {
        ecs_iter_t iter = ecs_filter_iter(it->world, get_filter(SHMUP_ECS_FILTER_PLAY_POS_VEL));
        while (ecs_filter_next(&iter)) {
            Player *player = ecs_field(&iter, Player, 1);      
            for (u8 i = 0; i < iter.count; i++) {
                player[i].input_active = false;
                break;
            }
        }

        create_menu_ui(it->world, menus, SHMUP_GAME2UI_ENUM_END, 56, callbacks);
        menu_visible = !menu_visible;
        // ToDo pause game
    }

    if (time_elasped > 4.0f && !spwned) {
        spawn_enemy(it->world, SHMUP_ENEMY_POPCORN2, (Vector2) { 110, 130});
        spwned = true;
    }

    // Todo update level time, spawn monster, powerups, ecc
    time_elasped += it->delta_time;
}

void game2_end(ecs_world_t *world)
{
    TraceLog(LOG_DEBUG, "game2 view end");
    menu_visible = false;
    level_t *level = get_current_level();
    if (level != NULL)
    {
        for (u16 i = 0; i < level->backgrounds_number; i++)
        {
            delete_entity_name(world, TextFormat("map%d", i));
        }
    }
    delete_menu_ui(world, menus, SHMUP_GAME2UI_ENUM_END);
    clear_level_entities(world);
}

void _ui_select_resume(ecs_world_t *world)
{
    ecs_iter_t iter = ecs_filter_iter(world, get_filter(SHMUP_ECS_FILTER_PLAY_POS_VEL));
    while (ecs_filter_next(&iter)) {
        Player *player = ecs_field(&iter, Player, 1);      
        for (u16 i = 0; i < iter.count; ++i) {
            player[i].input_active = true;
            break;
        }
    }
    delete_menu_ui(world, menus, SHMUP_GAME2UI_ENUM_END);
    menu_visible = !menu_visible;
}

void _ui_select_exit(ecs_world_t *world)
{
    set_current_view(world, SHMUP_VIEW_MENU);
}
