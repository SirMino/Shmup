#ifndef GAME2_VIEW_H
#define GAME2_VIEW_H

#include "../../include/flecs/flecs.h"

void game2_init(ecs_world_t *world);
void game2_update(ecs_iter_t *it);
void game2_end(ecs_world_t *world);

#endif
