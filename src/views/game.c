#include "../admins/entity.h"
#include "../admins/view.h"
#include "../admins/configuration.h"
#include "../admins/level.h"
#include "../admins/filters.h"

#include "../components/player.h"
#include "../components/input.h"
#include "../components/state.h"

#include "../enums/align.h"

#include "../../include/raylib/raymath.h"

#include "game.h"
#include <stdbool.h>

static enum menu
{
    SHMUP_GAMEUI_RESUME,
    SHMUP_GAMEUI_EXIT,
    SHMUP_GAMEUI_ENUM_END,
} InGameUI;

static char* menus[] =
    {
        "Resume",
        "Exit"
    };

static void _ui_select_resume(ecs_world_t *world);
static void _ui_select_exit(ecs_world_t *world);
static void (*callbacks[SHMUP_GAMEUI_ENUM_END])(ecs_world_t *world) = {_ui_select_resume, _ui_select_exit};

static bool menu_visible = false;

void game_init(ecs_world_t *world)
{
    level_t *level = set_level(SHMUP_LEVELS_1);
    if (level != NULL)
    {
        for (u8 i = 0; i < level->backgrounds_number; i++)
        {
            create_sprite2(world, TextFormat("map%d", i), level->backgrounds[i].texture, Vector2Zero());
        }

        create_player(world, "player", (Vector2) {level->spawn_point.x, level->spawn_point.y});
        create_follower(world, "follower", level->follower_point);
    }
}
void game_update(ecs_iter_t *it)
{
    State * state = ecs_singleton_get_mut(it->world, State);
    const Input * input = ecs_singleton_get(it->world, Input);

    if (input->debug)
    {
        state->debug = !state->debug;
    }

    if (input->esc && !menu_visible)
    {
        ecs_iter_t iter = ecs_filter_iter(it->world, get_filter(SHMUP_ECS_FILTER_PLAY_POS_VEL));
        while (ecs_filter_next(&iter)) {
            Player *player = ecs_field(&iter, Player, 1);      
            for (u8 i = 0; i < iter.count; i++) {
                player[i].input_active = false;
                break;
            }
        }

        create_menu_ui(it->world, menus, SHMUP_GAMEUI_ENUM_END, 56, callbacks);
        create_sprite(it->world, "game_overlay", SHMUP_TEXTURE_MENU_BG, Vector2Zero());
        menu_visible = !menu_visible;
    }

    // if player reach halt point
    // player->input_active = false

}
void game_end(ecs_world_t *world)
{
    level_t *level = get_current_level();
    if (level != NULL)
    {
        for (u8 i = 0; i < level->backgrounds_number; i++)
            delete_entity_name(world, TextFormat("map%d", i));
    }
    delete_menu_ui(world, menus, SHMUP_GAMEUI_ENUM_END);
    delete_entity_name(world, "player");
    delete_entity_name(world, "follower");
    delete_entity_name(world, "game_overlay");
    
    menu_visible = false;

/*     for (int i = 0; i < level->backgrounds_number; i++)
    {
        create_sprite2(world, TextFormat("map%d", i), level->backgrounds[i].texture, Vector2Zero());
    } */
}

void _ui_select_resume(ecs_world_t *world)
{
    ecs_iter_t iter = ecs_filter_iter(world, get_filter(SHMUP_ECS_FILTER_PLAY_POS_VEL));
    while (ecs_filter_next(&iter)) {
        Player *player = ecs_field(&iter, Player, 1);      
        for (u8 i = 0; i < iter.count; i++) {
            player[i].input_active = true;
            break;
        }
    }
    delete_menu_ui(world, menus, SHMUP_GAMEUI_ENUM_END);
    delete_entity_name(world, "game_overlay");
    menu_visible = !menu_visible;
}

void _ui_select_exit(ecs_world_t *world)
{
    set_current_view(world, SHMUP_VIEW_MENU);
}
