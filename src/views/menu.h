#ifndef MENU_VIEW_H
#define MENU_VIEW_H

#include "../../include/flecs/flecs.h"

void menu_init(ecs_world_t *world);
void menu_update(ecs_iter_t *it);
void menu_end(ecs_world_t *world);

#endif
