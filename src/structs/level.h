#ifndef LEVEL_STRUCT_H
#define LEVEL_STRUCT_H

#include "datatypes.h"
#include "../../include/raylib/raylib.h"

#include "../components/vectors.h"

typedef struct level_background
{
    Texture2D *texture;
    u8 order;
    bool repeat;
    f32 time;
    Vector2 frame_start;
    Vector2 frame_end;
} level_background_t;

struct level
{
    char *name;
    u8 backgrounds_number;
    level_background_t *backgrounds;
    Position follower_point;
    Position spawn_point;
    Position halt_point;
};

typedef struct level level_t;

#endif
