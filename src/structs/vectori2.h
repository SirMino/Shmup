#ifndef VECTORI2_H
#define VECTORI2_H

#include "datatypes.h"

#include "../../include/flecs/flecs.h"

struct vectori2
{
    i16 x, y;
};

typedef struct vectori2 VectorI2;

typedef VectorI2 IPosition;
ECS_COMPONENT_DECLARE(IPosition);


#endif
