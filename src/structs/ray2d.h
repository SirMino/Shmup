#ifndef RAY2D_H
#define RAY2D_H

#include "../../include/raylib/raylib.h"

typedef struct ray_2d {
    Vector2 origin;
    Vector2 direction;
} Ray2D;

#endif