#include <stdlib.h>

#include "warfare.h"

#include "../../include/cjson/cJSON.h"

#include "files.h"

#include "../components/weapon.h"
#include "../components/bullet.h"

static Weapon weapons[SHMUP_WEAPON_ENUM_END];
static Bullet bullets[SHMUP_BULLET_ENUM_END];

/* static Weapon _load_weapon(const char* weapon_file)
{
    Weapon weapon;
    const char *_weapon_file = load_text_file(weapon_file);
    cJSON *weapon_json = cJSON_Parse(_weapon_file);
    VectorI2 * noozles_array = NULL;
    i8 count = 0;

    if (weapon_json != NULL)
    {
        const cJSON *weapon_id= cJSON_GetObjectItemCaseSensitive(weapon_json, "id");
        const cJSON *weapon_name = cJSON_GetObjectItemCaseSensitive(weapon_json, "name");
        const cJSON *weapon_fi = cJSON_GetObjectItemCaseSensitive(weapon_json, "fire_interval");
        const cJSON *weapon_bullet_id = cJSON_GetObjectItemCaseSensitive(weapon_json, "bullet_id");
        const cJSON *weapon_speed = cJSON_GetObjectItemCaseSensitive(weapon_json, "speed");
        const cJSON *weapon_noozles_number= cJSON_GetObjectItemCaseSensitive(weapon_json, "noozles_number");
        const cJSON *weapon_noozles = cJSON_GetObjectItemCaseSensitive(weapon_json, "noozles");
        cJSON *noozle= NULL;
        if (cJSON_IsArray(weapon_noozles))
        {
            cJSON_ArrayForEach(noozle, weapon_noozles)
            {
                cJSON *directions = cJSON_GetObjectItemCaseSensitive(noozle, "directions");
                cJSON *direction_x = cJSON_GetObjectItemCaseSensitive(directions, "x");
                cJSON *direction_y = cJSON_GetObjectItemCaseSensitive(directions, "y");

                noozles_array = realloc(noozles_array, sizeof(VectorI2) * (weapon_noozles_number->valueint));
                noozles_array[count].x = direction_x->valueint;
                noozles_array[count].y = direction_y->valueint;
                count++;
            }
        }


        weapon.id = weapon_id->valueint;
        weapon.name  = malloc(TextLength(weapon_name->valuestring) * sizeof(char));
        TextCopy(weapon.name , weapon_name->valuestring);
        weapon.fire_interval = weapon_fi->valuedouble;
        weapon.bullet_id = weapon_bullet_id->valueint;
        weapon.speed = weapon_speed->valuedouble;
        weapon.noozles_number = weapon_noozles_number->valueint;
        weapon.noozles = malloc(sizeof(VectorI2) * weapon.noozles_number);
        for (int i = 0; i < weapon.noozles_number; ++i)
        {
            weapon.noozles[i] = noozles_array[i];
        }
    }

    free(noozles_array);
    cJSON_Delete(weapon_json);

    return weapon;
} */

static void _load_weapons()
{
    char *_weapon_file = load_text_file("res/data/weapons.json");
    cJSON *weapon_json = cJSON_Parse(_weapon_file);
    i8 count = 0;

    if (weapon_json != NULL)
    {
        const cJSON *weapons_array = cJSON_GetObjectItemCaseSensitive(weapon_json, "weapons");
        cJSON *weapon_it= NULL;
        if (cJSON_IsArray(weapons_array))
        {
            cJSON_ArrayForEach(weapon_it, weapons_array)
            {
                Weapon _weapon;
                VectorI2 * noozles_array = NULL;
                
                const cJSON *weapon_id= cJSON_GetObjectItemCaseSensitive(weapon_it, "id");
                const cJSON *weapon_name = cJSON_GetObjectItemCaseSensitive(weapon_it, "name");
                const cJSON *weapon_fi = cJSON_GetObjectItemCaseSensitive(weapon_it, "fire_interval");
                const cJSON *weapon_bullet_id = cJSON_GetObjectItemCaseSensitive(weapon_it, "bullet_id");
                const cJSON *weapon_speed = cJSON_GetObjectItemCaseSensitive(weapon_it, "speed");
                const cJSON *weapon_damage_multi = cJSON_GetObjectItemCaseSensitive(weapon_it, "damage_multiplier");
                const cJSON *weapon_noozles_number= cJSON_GetObjectItemCaseSensitive(weapon_it, "noozles_number");
                const cJSON *weapon_noozles = cJSON_GetObjectItemCaseSensitive(weapon_it, "noozles");
                cJSON *noozle= NULL;
                if (cJSON_IsArray(weapon_noozles))
                {
                    cJSON_ArrayForEach(noozle, weapon_noozles)
                    {
                        cJSON *directions = cJSON_GetObjectItemCaseSensitive(noozle, "directions");
                        cJSON *direction_x = cJSON_GetObjectItemCaseSensitive(directions, "x");
                        cJSON *direction_y = cJSON_GetObjectItemCaseSensitive(directions, "y");

                        noozles_array = realloc(noozles_array, sizeof(VectorI2) * (weapon_noozles_number->valueint));
                        noozles_array[count].x = direction_x->valueint;
                        noozles_array[count].y = direction_y->valueint;
                        count++;
                    }
                }

                _weapon.id = weapon_id->valueint;
                _weapon.name  = malloc(TextLength(weapon_name->valuestring) * sizeof(char));
                TextCopy(_weapon.name , weapon_name->valuestring);
                _weapon.fire_interval = weapon_fi->valuedouble;
                _weapon.bullet_id = weapon_bullet_id->valueint;
                _weapon.speed = weapon_speed->valuedouble;
                _weapon.damage_multiplier = weapon_damage_multi->valuedouble;
                _weapon.noozles_number = weapon_noozles_number->valueint;
                _weapon.noozles = malloc(sizeof(VectorI2) * _weapon.noozles_number);
                for (int i = 0; i < _weapon.noozles_number; ++i)
                {
                    _weapon.noozles[i] = noozles_array[i];
                }
                weapons[_weapon.id] = _weapon;

                free(noozles_array);
            }
        }
    }

    cJSON_Delete(weapon_json);
    free(_weapon_file);
}

static void _load_bullets()
{
    char *_bullets_file = load_text_file("res/data/bullets.json");
    cJSON *bullet_json = cJSON_Parse(_bullets_file);

    if (bullet_json != NULL)
    {
        const cJSON *bullets_array = cJSON_GetObjectItemCaseSensitive(bullet_json, "bullets");
        cJSON *bullet_it= NULL;
        if (cJSON_IsArray(bullets_array))
        {
            cJSON_ArrayForEach(bullet_it, bullets_array)
            {
                Bullet _bullet;
                _bullet.id = cJSON_GetObjectItemCaseSensitive(bullet_it, "id")->valueint;
                _bullet.texture = cJSON_GetObjectItemCaseSensitive(bullet_it, "texture")->valueint;
                _bullet.lifespan = cJSON_GetObjectItemCaseSensitive(bullet_it, "lifespan")->valuedouble;
                _bullet.damage = cJSON_GetObjectItemCaseSensitive(bullet_it, "damage")->valueint;

                bullets[_bullet.id] = _bullet;
            }
            cJSON_Delete(bullet_it);
        }
    }

    cJSON_Delete(bullet_json);
    free(_bullets_file);
}

/* static Bullet _load_bullet(const char* bullet_file)
{
    Bullet bullet;
    const char *_bullet_file = load_text_file(bullet_file);
    cJSON *bulletjson = cJSON_Parse(_bullet_file);

    if (bulletjson != NULL)
    {
        bullet.id = cJSON_GetObjectItemCaseSensitive(bulletjson, "id")->valueint;
        bullet.texture = cJSON_GetObjectItemCaseSensitive(bulletjson, "texture")->valueint;
        bullet.lifespan = cJSON_GetObjectItemCaseSensitive(bulletjson, "lifespan")->valuedouble;
        bullet.damage = cJSON_GetObjectItemCaseSensitive(bulletjson, "damage")->valueint;
    }

    cJSON_Delete(bulletjson);

    return bullet;
} */

static void weapons_init()
{
    _load_weapons();
    //weapons[SHMUP_WEAPON_GUN] = _load_weapon("res/data/weapons/gun.json");
    //weapons[SHMUP_WEAPON_LASER] = _load_weapon("res/data/weapons/laser.json");
}

static void bullets_init()
{
    _load_bullets();
    //bullets[SHMUP_BULLET_STANDARD] = _load_bullet("res/data/bullets/standard.json");
    //bullets[SHMUP_BULLET_STANDARD_STRONGER] = _load_bullet("res/data/bullets/blue.json");
}

void warfare_admin_init()
{
    // load weapons
    weapons_init();

    // load bullets
    bullets_init();
}


void warfare_admin_end()
{
/*      for (int i = 0; i < SHMUP_LEVELS_ENUM_END; i++)
     {
        free(levels[i].name);
        free(levels[i].backgrounds);
     } */
}

Weapon *get_weapon(WeaponType type)
{
    return &weapons[type];
}

Bullet *get_bullet(BulletType type)
{
    return &bullets[type];
}
