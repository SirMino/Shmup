#ifndef LEVEL_ADMIN_H
#define LEVEL_ADMIN_H

#include "../structs/level.h"

typedef enum levels
{
    SHMUP_LEVELS_1,
    SHMUP_LEVELS_2,
    SHMUP_LEVELS_ENUM_END
} Levels;

//level_t load_level(const char *level);

void levels_admin_init();
void levels_admin_end();
level_t *set_level(u8 level);
level_t *get_current_level();

#endif
