#ifndef AUDIO_ADMIN_H
#define AUDIO_ADMIN_H

#include "../../include/raylib/raylib.h"

typedef enum sfx_type
{
    SHMUP_SFX_PHASER,
    SHMUP_SFX_MINIGUN,
    SHMUP_SFX_ENUM_END
} SFXType;

void audio_admin_init();
void audio_admin_end();
Sound * get_sound(SFXType type);

#endif