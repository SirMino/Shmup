#ifndef WARFARE_ADMIN_H
#define WARFARE_ADMIN_H

#include "../components/weapon.h"
#include "../components/bullet.h"

typedef enum weapons
{
    SHMUP_WEAPON_GUN,
    SHMUP_WEAPON_LASER,
    SHMUP_WEAPON_ENUM_END
} WeaponType;

typedef enum bullets
{
    SHMUP_BULLET_STANDARD,
    SHMUP_BULLET_STANDARD_STRONGER,
    SHMUP_BULLET_3,
    SHMUP_BULLET_ENUM_END
} BulletType;

void warfare_admin_init();
void warfare_admin_end();
Weapon *get_weapon(WeaponType type);
Bullet *get_bullet(BulletType type);

#endif