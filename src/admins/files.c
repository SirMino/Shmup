#include "files.h"

#include <stdlib.h>
#include <stdio.h>

char * load_text_file(const char *file)
{
    FILE * _file;
    char * buffer = 0;
    long length;
    _file = fopen(file, "r");

    if (_file)
    {
        fseek(_file, 0, SEEK_END);
        length = ftell(_file);

        fseek(_file, 0, SEEK_SET);
        buffer = malloc(length);

        if (buffer)
        {
            fread(buffer, 1, length, _file);
        }

        fclose(_file);
    }

    return buffer;
}