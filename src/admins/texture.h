#ifndef TEXTURE_ADMIN_H
#define TEXTURE_ADMIN_H

#include "../../include/raylib/raylib.h"

//#define TOTAL_TEXTURES 2

typedef enum texture_type
{
    SHMUP_TEXTURE_MENU_BG,
    SHMUP_TEXTURE_SCROLLING_BG,
    SHMUP_TEXTURE_SHIP,
    SHMUP_TEXTURE_MAP1,
    SHMUP_TEXTURE_ARROW,
    SHMUP_TEXTURE_CROSS,
    SHMUP_TEXTURE_BULLET1,
    SHMUP_TEXTURE_BULLET2,
    SHMUP_TEXTURE_OVERLAY,
    SHMUP_TEXTURE_SCROLLING_CLOUDS,
    SHMUP_TEXTURE_SHIP_2,
    SHMUP_TEXTURE_ENEMY_1,
    SHMUP_TEXTURE_BULLET3,
    SHMUP_TEXTURE_EXPLOSIOND,
    SHMUP_TEXTURE_DRONE,
    SHMUP_TEXTURE_ENEMY_2,
    SHMUP_TEXTURE_ENUM_END
} TextureType;

void texture_admin_init();
void texture_admin_end();

RenderTexture2D * get_viewport();
Texture2D * get_texture(TextureType type);

#endif
