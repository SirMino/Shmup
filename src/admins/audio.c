#include <stdio.h>

#include "../structs/datatypes.h"
#include "audio.h"
//#define NULL ((void*)0)

Sound _sounds[SHMUP_SFX_ENUM_END];

void audio_admin_init()
{
    _sounds[SHMUP_SFX_PHASER] =  LoadSound("res/sounds/phaserUp6.mp3");
    _sounds[SHMUP_SFX_MINIGUN] =  LoadSound("res/sounds/minigun.ogg");

}

void audio_admin_end() {

    for (u8 i = 0; i < SHMUP_SFX_ENUM_END; ++i)
        UnloadSound(_sounds[i]);
}

Sound * get_sound(SFXType type) {
    if (type < SHMUP_SFX_ENUM_END)
        return &_sounds[type];

    return NULL;
}