#ifndef VIEW_ADMIN_H
#define VIEW_ADMIN_H

#include "../components/view.h"

typedef enum view_type
{
    SHMUP_VIEW_MENU,
    SHMUP_VIEW_GAME,
    SHMUP_VIEW_GAME2,
    SHMUP_VIEW_ENUM_END
} ViewType;

void set_current_view(ecs_world_t *world, int view);

void view_admin_init(ecs_world_t *world);
void view_admin_end(ecs_world_t *world);

View * get_current_view();

#endif