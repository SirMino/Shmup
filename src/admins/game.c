// libs
#include "../../include/raylib/raylib.h"
#include "../../include/flecs/flecs.h"

// Admins
//#include "lua.h"
#include "configuration.h"
#include "component.h"
#include "entity.h"
#include "engine.h"
#include "filters.h"
#include "texture.h"
#include "animation.h"
#include "view.h"
#include "level.h"
#include "warfare.h"
#include "audio.h"

// Components
#include "../components/screen.h"
#include "../components/state.h"
#include "../components/input.h"

#include "../globals.h"
#include "game.h"

static ecs_world_t *_world = NULL;

static inline void _raylib_init()
{
    game_config_t *config = get_configuration();
    InitWindow(config->render_resolution.width, config->render_resolution.height, config->game_name);
    SetTargetFPS(config->fps);
    SetTraceLogLevel(LOG_DEBUG);
    SetExitKey(KEY_NULL);
    SetWindowState(FLAG_WINDOW_RESIZABLE);
    InitAudioDevice();
    //SetMasterVolume(1.0);

    // todo font manager kinda like texture manager
    //config->font = LoadFontEx("res/fonts/CodeNewRoman.otf", 32, NULL, 0);
    //SetTextureFilter(config->font.texture, TEXTURE_FILTER_BILINEAR);
}

static inline void _flecs_init()
{
    _world = ecs_init();
    // for explorer https://flecs.dev/explorer
    //ecs_singleton_set(_world, EcsRest, {0});
}

static inline void _game_init()
{
    game_config_t *config = get_configuration();
    ecs_singleton_set(_world, GameScreen, { (Rectangle) {0,0,config->native_resolution.width,config->native_resolution.height}, (Rectangle) {0,0,config->render_resolution.width,config->render_resolution.height} });
    ecs_singleton_set(_world, State, { false, false });
    ecs_singleton_set(_world, Input, { false, false, false });
    View * _view = ecs_singleton_get_mut(_world, View);
    _view->init_void(_world);
}

static inline void _admins_init()
{
    configuration_admin_postraylib_init();
    component_admin_init(_world);
    entity_admin_init(_world);
    engine_admin_init(_world);
    texture_admin_init();
    animations_admin_init();
    levels_admin_init();
    warfare_admin_init();
    view_admin_init(_world);
    audio_admin_init();
    filters_admin_init(_world);

    //get_current_screen()->init_void(_world);
}

void game_admin_init()
{
    // Init configuration
    configuration_admin_init();

    // Init engine frameworks
    _raylib_init();
    _flecs_init();
    //lua_admin_init();
    _admins_init();
    _game_init();
}

void game_admin_loop()
{
    const game_config_t *config = get_configuration();

    while (config->running && !WindowShouldClose())
    {
        // should not be inside the begin drawing?
        ecs_progress(_world, GetFrameTime());
    }
}

static inline void _raylib_end()
{
    CloseWindow();
    CloseAudioDevice();
}

static inline void _flecs_end()
{
    ecs_fini(_world);
}

static inline void _admins_end()
{
    View * _view = ecs_singleton_get_mut(_world, View);
    _view->end_void(_world);

    texture_admin_end();
    levels_admin_end();
    warfare_admin_end();
    animations_admin_end();
    configuration_admin_end();
    view_admin_end(_world);
    audio_admin_end();
    filters_admin_end();
}

void game_admin_end()
{
    // End engine frameworks
    _admins_end();
    _raylib_end();
    _flecs_end();
    //lua_admin_end();
}
