#include "../../include/raylib/raylib.h"

#include "configuration.h"
#include "texture.h"

static Texture2D _textures[SHMUP_TEXTURE_ENUM_END];
static RenderTexture2D _viewport;

void texture_admin_init()
{
    game_config_t *config = get_configuration();
    _viewport = LoadRenderTexture(config->native_resolution.width, config->native_resolution.height);

    _textures[SHMUP_TEXTURE_MENU_BG] = LoadTexture("res/sprites/menu_bg.png");
    _textures[SHMUP_TEXTURE_SCROLLING_BG] = LoadTexture("res/sprites/scrolling_bg.png");
    _textures[SHMUP_TEXTURE_SHIP] = LoadTexture("res/sprites/ship1.png");
    _textures[SHMUP_TEXTURE_MAP1] = LoadTexture("res/sprites/levels/map1.png");
    _textures[SHMUP_TEXTURE_ARROW] = LoadTexture("res/sprites/arrow.png");
    _textures[SHMUP_TEXTURE_CROSS] = LoadTexture("res/sprites/cross.png");
    _textures[SHMUP_TEXTURE_BULLET1] = LoadTexture("res/sprites/bullet.png");
    _textures[SHMUP_TEXTURE_BULLET2] = LoadTexture("res/sprites/bullet2.png");
    _textures[SHMUP_TEXTURE_BULLET3] = LoadTexture("res/sprites/bullet3.png");

    Image bkg = GenImageColor(240, 320, BLACK);
    _textures[SHMUP_TEXTURE_OVERLAY] = LoadTextureFromImage(bkg);
    UnloadImage(bkg);
    
    _textures[SHMUP_TEXTURE_SCROLLING_CLOUDS] = LoadTexture("res/sprites/scrolling_bg_clouds.png");
    _textures[SHMUP_TEXTURE_SHIP_2] = LoadTexture("res/sprites/shipsheet.png");
    _textures[SHMUP_TEXTURE_ENEMY_1] = LoadTexture("res/sprites/enemy1.png");
    _textures[SHMUP_TEXTURE_ENEMY_2] = LoadTexture("res/sprites/enemy2.png");
    _textures[SHMUP_TEXTURE_EXPLOSIOND] = LoadTexture("res/sprites/explosiond.png");
    _textures[SHMUP_TEXTURE_DRONE] = LoadTexture("res/sprites/Drone-sheet1.png");
}

void texture_admin_end()
{
    for (int i = 0; i < SHMUP_TEXTURE_ENUM_END; i++)
        UnloadTexture(_textures[i]);

    UnloadRenderTexture(_viewport);
}

Texture2D * get_texture(TextureType type)
{
    return & _textures[type];
}

RenderTexture2D * get_viewport()
{
  return &_viewport;
}
