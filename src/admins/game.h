#ifndef GAME_ADMIN_H
#define GAME_ADMIN_H

void game_admin_init();
void game_admin_loop();
void game_admin_end();

#endif