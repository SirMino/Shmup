#include "filters.h"

#include "../components/sprite.h"
#include "../components/animation.h"
#include "../components/vectors.h"
#include "../components/shapes.h"
#include "../components/screen.h"
#include "../components/text.h"
#include "../components/player.h"
#include "../components/state.h"
#include "../components/ui.h"
#include "../components/input.h"
#include "../components/speed.h"
#include "../components/bullet.h"
#include "../components/tags.h"

static ecs_filter_t filters[SHMUP_ECS_FILTER_ENUM_END];

void filters_admin_init(ecs_world_t *world)
{
    filters[SHMUP_ECS_FILTER_FOLL_POS] = *ecs_filter(world, {
        .terms = {
            { ecs_id(Follower) },
            { ecs_id(Position) }
            //{ ecs_id(Velocity) }
        }
    });
    filters[SHMUP_ECS_FILTER_PLAY_POS_VEL] = *ecs_filter(world, {
        .terms = {
            { ecs_id(Player) },
            { ecs_id(Position) },
            { ecs_id(Velocity) }
        }
    });
    filters[SHMUP_ECS_FILTER_ROLLINGBG] = *ecs_filter(world, {
        .terms = {
            { ecs_id(RollingBackground) },
            { ecs_id(Position) },
            { ecs_id(TimeFlow) },
            { ecs_id(Sprite) }
        }
    });
    filters[SHMUP_ECS_FILTER_BULLETS] = *ecs_filter(world, {
        .terms = {
            { ecs_id(Bullet) }
        }
    });
    filters[SHMUP_ECS_FILTER_BB] = *ecs_filter(world, {
        .terms = {
            { ecs_id(BoundingBox2D) },
            { ecs_id(Position) }
        }
    });
    filters[SHMUP_ECS_FILTER_BB_NOTBULLETS] = *ecs_filter(world, {
        .terms = {
            { ecs_id(BoundingBox2D) },
            { ecs_id(Position) },
            { ecs_id(Bullet), .oper = EcsNot}
        }
    });
    filters[SHMUP_ECS_FILTER_ENEMY] = *ecs_filter(world, {
        .terms = {
            { ecs_id(Enemy) },
            { ecs_id(Position) },
            { ecs_id(BoundingBox2D) },
            { ecs_id(Sprite) },
            { ecs_id(Animation) }
        }
    });
}

void filters_admin_end()
{
    // free ?
    //for (uint16_t i = 0; i < SHMUP_ECS_FILTER_ENUM_END; ++i)
    //    MemFree(&filters[i]);

    //MemFree(filters);
}

ecs_filter_t *get_filter(int filter)
{
    return &filters[filter];
}
