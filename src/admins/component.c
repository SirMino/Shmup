#include "../components/vectors.h"
#include "../components/shapes.h"
#include "../components/speed.h"
#include "../components/sprite.h"
#include "../components/text.h"
#include "../components/view.h"
#include "../components/screen.h"
#include "../components/player.h"
#include "../components/enemy.h"
#include "../components/weapon.h"
#include "../components/bullet.h"
#include "../components/sound.h"
#include "../components/particle.h"
#include "../components/animation.h"
#include "../components/input.h"
#include "../components/state.h"
#include "../components/ui.h"
#include "../components/tags.h"

//#include "../structs/vectori2.h"

#include "component.h"

void component_admin_init(ecs_world_t *world)
{
    // Components
    ECS_COMPONENT_DEFINE(world, Position);
    ECS_COMPONENT_DEFINE(world, Direction);
    ECS_COMPONENT_DEFINE(world, Velocity);
    ECS_COMPONENT_DEFINE(world, Speed);
    ECS_COMPONENT_DEFINE(world, TimeFlow);
    ECS_COMPONENT_DEFINE(world, Sprite);
    ECS_COMPONENT_DEFINE(world, RollingBackground);
    ECS_COMPONENT_DEFINE(world, RenderText);
    ECS_COMPONENT_DEFINE(world, View);
    ECS_COMPONENT_DEFINE(world, GameScreen);
    ECS_COMPONENT_DEFINE(world, Input);
    ECS_COMPONENT_DEFINE(world, State);
    ECS_COMPONENT_DEFINE(world, Player);
    ECS_COMPONENT_DEFINE(world, ShmEnemy);
    ECS_COMPONENT_DEFINE(world, Weapon);
    ECS_COMPONENT_DEFINE(world, Bullet);
    ECS_COMPONENT_DEFINE(world, ShmSound);
    ECS_COMPONENT_DEFINE(world, ButtonText);
    ECS_COMPONENT_DEFINE(world, Animation);
    ECS_COMPONENT_DEFINE(world, BoundingBox2D);

    ECS_COMPONENT_DEFINE(world, Particle);
    ECS_COMPONENT_DEFINE(world, ParticleEmitter);

    // Tags
    ECS_TAG_DEFINE(world, Follower);
    ECS_TAG_DEFINE(world, UI);
    ECS_TAG_DEFINE(world, RollingBg);
    ECS_TAG_DEFINE(world, Enemy);
    ECS_TAG_DEFINE(world, PlayerTag);
}
