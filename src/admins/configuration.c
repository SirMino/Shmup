#include <stdio.h>
#include <stdlib.h>

#include "configuration.h"

#include "../../include/cjson/cJSON.h"
#include "../../include/raylib/raylib.h"

static game_config_t * config;

static char * _load_config_file()
{
    FILE * config_file;
    char * buffer = 0;
    long length;
    config_file = fopen("./res/configuration.json", "r");

    if (config_file)
    {
        fseek(config_file, 0, SEEK_END);
        length = ftell(config_file);

        fseek(config_file, 0, SEEK_SET);
        buffer = malloc(length);

        if (buffer)
        {
            fread(buffer, 1, length, config_file);
        }

    }

    fclose(config_file);
    return buffer;
}

static void _load_font()
{
    config->font = LoadFontEx(TextFormat("res/fonts/%s", config->font_name), config->font_base_size, NULL, 0);
    SetTextureFilter(config->font.texture, config->font_filter);
}

void configuration_admin_init()
{
    config = malloc(sizeof * config);

    // load json file
    char *json_string = _load_config_file();

    cJSON *json = cJSON_Parse(json_string);

    // set configuration struct
    if (json != NULL)
    {
        const cJSON *game = cJSON_GetObjectItemCaseSensitive(json, "game");
        const cJSON *game_name = cJSON_GetObjectItemCaseSensitive(game, "name");
        if (cJSON_IsString(game_name) && (game_name->valuestring != NULL))
        {
            config->game_name = malloc(TextLength(game_name->valuestring) * sizeof(char));
            TextCopy(config->game_name, game_name->valuestring);
        }
        config->fps = cJSON_GetObjectItemCaseSensitive(game, "fps")->valueint;

        const cJSON *native_resolution = cJSON_GetObjectItemCaseSensitive(game, "native_resolution");
        config->native_resolution.width = cJSON_GetObjectItemCaseSensitive(native_resolution, "width")->valueint;
        config->native_resolution.height = cJSON_GetObjectItemCaseSensitive(native_resolution, "height")->valueint;

        const cJSON *render_resolution = cJSON_GetObjectItemCaseSensitive(game, "render_resolution");
        config->render_resolution.width = cJSON_GetObjectItemCaseSensitive(render_resolution, "width")->valueint;
        config->render_resolution.height = cJSON_GetObjectItemCaseSensitive(render_resolution, "height")->valueint;     

        const cJSON *font_name = cJSON_GetObjectItemCaseSensitive(game, "font_name");
        const cJSON *font_filter = cJSON_GetObjectItemCaseSensitive(game, "font_filter");
        const cJSON *font_base_size = cJSON_GetObjectItemCaseSensitive(game, "font_base_size");
        config->font_name = malloc(TextLength(font_name->valuestring) * sizeof(char));
        TextCopy(config->font_name, font_name->valuestring);
        config->font_filter = font_filter->valueint;
        config->font_base_size = font_base_size->valueint;

        const cJSON *palette_colors_number = cJSON_GetObjectItemCaseSensitive(game, "palette_colors");
        config->palette = malloc(sizeof(Color) * palette_colors_number->valueint);
        const cJSON *palette = cJSON_GetObjectItemCaseSensitive(game, "palette");
        cJSON *color = NULL;
        int8_t count = 0;
        if (cJSON_IsArray(palette))
        {
            cJSON_ArrayForEach(color, palette)
            {
                config->palette[count].r = cJSON_GetObjectItemCaseSensitive(color, "r")->valueint;
                config->palette[count].g = cJSON_GetObjectItemCaseSensitive(color, "g")->valueint;
                config->palette[count].b = cJSON_GetObjectItemCaseSensitive(color, "b")->valueint;
                config->palette[count].a = cJSON_GetObjectItemCaseSensitive(color, "a")->valueint;
                count++;
            }
        }

        config->running = true;

        const cJSON *custom_props = cJSON_GetObjectItemCaseSensitive(game, "custom_properties");
        config->custom_prop1 = cJSON_GetObjectItemCaseSensitive(custom_props, "custom_prop1")->valueint;
    }

    cJSON_Delete(json);
    free(json_string);
}

void configuration_admin_postraylib_init()
{
    _load_font();

    //TODO check video resolution and scale game accordingly
}

game_config_t * get_configuration()
{
    return config;
}

void configuration_admin_end()
{
    UnloadFont(config->font);
    free(config->game_name);
    free(config->font_name);
    free(config->palette);
    free(config);
}

