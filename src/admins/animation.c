#include "animation.h"

#include "../components/animation.h"
#include <stdlib.h>

static Animation animations[SHMUP_ANIMATIONS_ENUM_END];

static Animation _load_animation_idle()
{
    Animation anim = {};
    anim.animation_speed = 0.3;
    anim.frame_timer = 0.0;
    anim.current_frame = 0;
    anim.frames_number = 2;
    // anim.frames = realloc(anim.frames, sizeof(AnimationFrame) * 2);
    anim.frames = malloc(sizeof(AnimationFrame) * 2);
    anim.frames[0] = (AnimationFrame) {{0,0}, {32,44},1};
    anim.frames[1] = (AnimationFrame) { {32,0},{32,44},2};
    anim.flip_x = false;
    anim.id = SHMUP_ANIMATIONS_SHIP_IDLE;
    anim.loop = true;

    return anim;
}

static Animation _load_animation_strafe()
{
    Animation anim = {};
    anim.animation_speed = 0.3;
    anim.frame_timer = 0.0;
    anim.current_frame = 0;
    anim.frames_number = 2;
    // anim.frames = realloc(anim.frames, sizeof(AnimationFrame) * 2);
    anim.frames = malloc( sizeof(AnimationFrame) * 2);
    anim.frames[0] = (AnimationFrame) {{64,0}, {32,44}, 1};
    anim.frames[1] = (AnimationFrame) {{96,0}, {32,44},2};
    anim.flip_x = false;
    anim.id = SHMUP_ANIMATIONS_SHIP_STRAFE;
    anim.loop = true;

    return anim;
}

static Animation _load_animation_drone()
{
    Animation anim = {};
    anim.animation_speed = 0.1;
    anim.frame_timer = 0.0;
    anim.current_frame = 0;
    anim.frames_number = 5;
    anim.frames = malloc(sizeof(AnimationFrame) * 5);
    anim.frames[0] = (AnimationFrame) {{0,0}, {52,35},1};
    anim.frames[1] = (AnimationFrame) { {52,0},{52,35},2};
    anim.frames[2] = (AnimationFrame) { {104,0},{52,35},3};
    anim.frames[3] = (AnimationFrame) { {156,0},{52,35},4};
    anim.frames[4] = (AnimationFrame) { {208,0},{52,35},5};
    anim.flip_x = false;
    anim.id = SHMUP_ANIMATIONS_DRONE;
    anim.loop = true;

    return anim;
}

static Animation _load_animation_enemy1()
{
    Animation anim = {};
    anim.animation_speed = 0.4;
    anim.frame_timer = 0.0;
    anim.current_frame = 0;
    anim.frames_number = 4;
    anim.frames = malloc( sizeof(AnimationFrame) * 4);
    anim.frames[0] = (AnimationFrame) {{0,0}, {32,32},1};
    anim.frames[1] = (AnimationFrame) {{32,0}, {32,32},2};
    anim.frames[2] = (AnimationFrame) {{64,0}, {32,32},3};
    anim.frames[3] = (AnimationFrame) {{96,0}, {32,32},4};
    anim.flip_x = false;
    anim.id = SHMUP_ANIMATIONS_ENEMY1;
    anim.loop = true;

    return anim;
}

static Animation _load_animation_enemy2()
{
    const u8 frame_width = 16;
    const u8 frame_height = 38;
    Animation anim = {};
    anim.animation_speed = 0.2;
    anim.frame_timer = 0.0;
    anim.current_frame = 0;
    anim.frames_number = 4;
    anim.frames = malloc( sizeof(AnimationFrame) * 4);
    for (u8 i = 0; i < 4; ++i) {
        anim.frames[i] = (AnimationFrame) { { frame_width * i, 0 }, { frame_width, frame_height } };
    }
    anim.flip_x = false;
    anim.id = SHMUP_ANIMATIONS_ENEMY2;
    anim.loop = true;

    return anim;
}

static Animation _load_animation_explosiond()
{
    Animation anim = {};
    anim.animation_speed = 0.05;
    anim.frame_timer = 0.0;
    anim.current_frame = 0;
    anim.frames_number = 13;
    anim.frames = malloc( sizeof(AnimationFrame) * 13);
    for (u8 i = 0; i < 13; ++i)
    {
        anim.frames[i] = (AnimationFrame) {{i*96,0}, {96,96}, i+1};
    }
    //anim.frames[0] = (AnimationFrame) {{0,0}, {96,96}, 0.0, 1};
    anim.flip_x = false;
    anim.id = SHMUP_ANIMATIONS_EXPLOSIOND;
    anim.loop = false;

    return anim;
}

void animations_admin_init()
{
    animations[SHMUP_ANIMATIONS_SHIP_IDLE] = _load_animation_idle();
    animations[SHMUP_ANIMATIONS_SHIP_STRAFE] = _load_animation_strafe();
    animations[SHMUP_ANIMATIONS_ENEMY1] = _load_animation_enemy1();
    animations[SHMUP_ANIMATIONS_ENEMY2] = _load_animation_enemy2();
    animations[SHMUP_ANIMATIONS_EXPLOSIOND] = _load_animation_explosiond();
    animations[SHMUP_ANIMATIONS_DRONE] = _load_animation_drone();
}

Animation *get_animation(u16 animation)
{
    animations[animation].current_frame = 0;
    animations[animation].frame_timer = 0;
    //for (u16 i = 0; i < animations[animation].frames_number; ++i)
    //    animations[animation].frames[i].elapsed_time = 0.0f;

    return &animations[animation];
}

void animations_admin_end()
{
    for (u16 i = 0; i < SHMUP_ANIMATIONS_ENUM_END; ++i)
    {
        free(animations[i].frames);
    }

    //free(animations);
}
