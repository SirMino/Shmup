#include "../globals.h"
#include "../components/vectors.h"
#include "../components/shapes.h"
#include "../components/speed.h"
#include "../components/sprite.h"
#include "../components/text.h"
#include "../components/player.h"
#include "../components/enemy.h"
#include "../components/weapon.h"
#include "../components/sound.h"
#include "../components/animation.h"
#include "../components/tags.h"
#include "../components/ui.h"

#include "../admins/filters.h"
#include "../admins/warfare.h"
#include "../admins/audio.h"

#include "animation.h"
#include "entity.h"
#include "texture.h"

#include "../../include/raylib/raymath.h"

static inline ecs_entity_t create_entity(ecs_world_t *world)
{
    ecs_entity_t _entity = ecs_new_id(world);
    return _entity;
}

void entity_admin_init(ecs_world_t *world)
{
    //create_background(world);
}

void create_sprite(ecs_world_t * world, const char *name, TextureType sprite, Vector2 position)
{
    Rectangle frame = {0,0,get_texture(sprite)->width,get_texture(sprite)->height};
    ecs_entity_t e = ecs_new_id(world);
    ecs_set_name(world, e, name);
    ecs_set(world, e, Position, { position.x, position.y, });
    ecs_set(world, e, Sprite, { get_texture(sprite), frame, {0,0}, false } );
}

void create_sprite2(ecs_world_t * world, const char *name, Texture * sprite, Vector2 position)
{
    Rectangle frame = {0,0,sprite->width,sprite->height};
    ecs_entity_t e = ecs_new_id(world);
    ecs_set_name(world, e, name);
    ecs_set(world, e, Position, { position.x, position.y });
    ecs_set(world, e, Sprite, { sprite, frame, {0,0}, false } );
}

void create_rolling_bg(ecs_world_t * world, const char *name, level_background_t *level_bg, Vector2 position)
{
    Rectangle frame = { level_bg->frame_start.x, level_bg->frame_start.y,level_bg->texture->width,level_bg->texture->height};
    ecs_entity_t e = ecs_new_id(world);
    ecs_set_name(world, e, name);
    //ecs_add(world, e, RollingBg);
    ecs_set(world, e, RollingBackground, { level_bg->frame_start, level_bg->frame_end});
    ecs_set(world, e, Position, { position.x, position.y });
    ecs_set(world, e, Sprite, { level_bg->texture, frame, {0,0}, false } );
    ecs_set(world, e, TimeFlow, { 0.0, level_bg->time });
}

void create_text(ecs_world_t *world, const char *name, const char *text, u16 font_size, f32 pos_x, f32 pos_y, bool selected, bool highlight, enum Align h_align, enum Align v_align, Color color)
{
    ecs_entity_t e = ecs_new_id(world);
    ecs_set_name(world, e, name);
    ecs_set(world, e, RenderText, { text, font_size, selected, highlight, h_align, v_align, color });
    ecs_set(world, e, Position, { pos_x, pos_y });
}

void create_button_text(ecs_world_t *world, const char *name, const char *text, f32 x, f32 y, void (* callback)(ecs_world_t *world))
{
    ecs_entity_t e = ecs_new_id(world);
    ecs_set_name(world, e, name);
    ecs_add(world, e, UI);
    ecs_set(world, e, ButtonText, { text, callback });
    ecs_set(world, e, Position, { x, y });
}

void create_menu_ui(ecs_world_t *world, char* menus[], u8 num, u8 start_y, void (* callbacks[])(ecs_world_t *world))
{
    const u8 _x = 20;
    u8 _y = start_y;
    for(u8 i = 0; i < num; i++)
    {
        create_button_text(world, TextFormat("txt_%s", TextToLower(menus[i])), menus[i], _x, _y, *callbacks[i]);
        _y += 32;
    }
}

void delete_menu_ui(ecs_world_t *world, char* menus[], u8 num)
{
    for(u8 i = 0; i < num; i++)
    {
        delete_entity_name(world, TextFormat("txt_%s", TextToLower(menus[i])));
    }
}

void create_player(ecs_world_t *world, const char *name, Vector2 position)
{
    Texture *ship_texture = get_texture(SHMUP_TEXTURE_SHIP_2);
    Animation *anim = get_animation(SHMUP_ANIMATIONS_SHIP_IDLE);
    // Rectangle frame = {0,0,ship_texture->width,ship_texture->height};
    Rectangle frame = {anim->frames[0].coords.x,anim->frames[0].coords.y,anim->frames[0].size.x, anim->frames[0].size.y};
    Weapon *gun = get_weapon(SHMUP_WEAPON_LASER);
    ecs_entity_t e = ecs_new_id(world);
    ecs_set_name(world, e, name);
    ecs_add(world, e, PlayerTag);
    // ecs_set(world, e, Player, { .camera={ {0,0}, position, 0.0, 1.0 }, .life=100, .shields=100 });
    ecs_set(world, e, Player, { .life=100, .shields=100, .input_active=true });
    ecs_set(world, e, Position, { position.x, position.y });
    ecs_set(world, e, Direction, { 0.0, 0.0 });
    ecs_set(world, e, Velocity, { 0, 0 });
    ecs_set(world, e, Speed, { 100.0, 300.0 });
    ecs_set(world, e, Sprite, { ship_texture, frame, {0,0}, true } );
    ecs_set(world, e, Weapon, { gun->id, 
                                gun->name, 
                                gun->fire_interval, 
                                gun->bullet_id, 
                                gun->speed, 
                                gun->damage_multiplier, 
                                gun->noozles_number, 
                                gun->noozles});
    ecs_set(world, e, Animation, { anim->animation_speed, anim->frame_timer, anim->current_frame, anim->frames_number, anim->frames, .loop = anim->loop } );
    ecs_set(world, e, BoundingBox2D, {0,0,frame.width,frame.height});
}

void spawn_enemy(ecs_world_t *world, EnemyType type, Vector2 position)
{
    Texture *enemy_texture;
    Animation *anim;

    switch (type) {
        case SHMUP_ENEMY_POPCORN1:
            enemy_texture = get_texture(SHMUP_TEXTURE_ENEMY_1);
            anim = get_animation(SHMUP_ANIMATIONS_ENEMY1);
        break;
        case SHMUP_ENEMY_POPCORN2:
            enemy_texture = get_texture(SHMUP_TEXTURE_ENEMY_2);
            anim = get_animation(SHMUP_ANIMATIONS_ENEMY2);
        break;
    }

    Rectangle frame = {anim->frames[0].coords.x,anim->frames[0].coords.y,anim->frames[0].size.x, anim->frames[0].size.y};

    ecs_entity_t e = ecs_new_id(world);
    ecs_add(world, e, Enemy);
    ecs_set(world, e, ShmEnemy, { type });
    ecs_set(world, e, Position, { position.x, position.y });
    ecs_set(world, e, Velocity, {0});
    ecs_set(world, e, Speed, { 80.0, 80.0 });
    ecs_set(world, e, Sprite, { enemy_texture, frame, {0,0}, true } );
    ecs_set(world, e, Animation, { anim->animation_speed, anim->frame_timer, GetRandomValue(0, anim->frames_number-1), anim->frames_number, anim->frames, .loop = anim->loop } );
    ecs_set(world, e, BoundingBox2D, {0,0,frame.width,frame.height});
}

void spawn_explosion(ecs_world_t *world, Position pos)
{
    Texture *texture = get_texture(SHMUP_TEXTURE_EXPLOSIOND);
    Animation *anim = get_animation(SHMUP_ANIMATIONS_EXPLOSIOND);
    Rectangle frame = {anim->frames[0].coords.x,anim->frames[0].coords.y,anim->frames[0].size.x, anim->frames[0].size.y};
    
    ecs_entity_t e = ecs_new_id(world);
    ecs_set(world, e, Position, { pos.x - 48, pos.y -48 });
    ecs_set(world, e, Sprite, { texture, frame, {0,0}, false } );
    ecs_set(world, e, Animation, { anim->animation_speed,  anim->frame_timer, anim->current_frame, anim->frames_number, anim->frames, .loop = anim->loop, .destroy_after_run=true } );
}

void create_follower(ecs_world_t *world, const char *name, Position position)
{
    ecs_entity_t e = ecs_new_id(world);
    ecs_set_name(world, e, name);
    ecs_add(world, e, Follower);
    ecs_set(world, e, TimeFlow, { 0.0, 16.0 });
    ecs_set(world, e, Position, { position.x, position.y });
    //ecs_set(world, e, Velocity, { 0, 0 });
    //ecs_set(world, e, Speed, { 50.0, 50.0 });
    //ecs_set(world, e, Sprite, { get_texture(SHMUP_TEXTURE_ARROW), (VectorI2) {-8,-8}, false }); // TMP
}

// TODO provare flecs relationship https://ajmmertens.medium.com/building-games-in-ecs-with-entity-relationships-657275ba2c6c
void fire_bullet(ecs_world_t *world, Position position, f32 speed, Direction direction, u16 id)
{
    Bullet *bullet = get_bullet(id);
    Rectangle frame = {0,0,get_texture(bullet->texture)->width,get_texture(bullet->texture)->height};
    f32 pan = 1.0 - (position.x / 240);
    ecs_entity_t e = ecs_new_id(world);
    ecs_set(world, e, Bullet, {id, bullet->texture, bullet->lifespan, bullet->damage});
    ecs_set(world, e, Position, { position.x, position.y });
    ecs_set(world, e, Direction, { direction.x, direction.y });
    ecs_set(world, e, Velocity, { 0, 0 });
    ecs_set(world, e, Speed, { speed, speed });
    ecs_set(world, e, Sprite, { get_texture(bullet->texture), frame, {0,0}, false } );
    ecs_set(world, e, ShmSound, { SHMUP_SFX_MINIGUN, false, pan });
    ecs_set(world, e, BoundingBox2D, {0,0,frame.width,frame.height});
}

void clear_level_entities(ecs_world_t *world) {
    
    // Player
    ecs_iter_t player_iter = ecs_filter_iter(world, get_filter(SHMUP_ECS_FILTER_PLAY_POS_VEL));
    while (ecs_filter_next(&player_iter)) {
        for (u16 i = 0; i < player_iter.count; ++i)
            delete_entity_id(world, player_iter.entities[i]);
    }

    // bullets
	ecs_iter_t bullet_iter = ecs_filter_iter(world, get_filter(SHMUP_ECS_FILTER_BULLETS));
    while (ecs_filter_next(&bullet_iter)) {
        for (u16 i = 0; i < bullet_iter.count; ++i)
            delete_entity_id(world, bullet_iter.entities[i]);
    }

    // enemies
	ecs_iter_t enemy_iter = ecs_filter_iter(world, get_filter(SHMUP_ECS_FILTER_ENEMY));
    while (ecs_filter_next(&enemy_iter)) {
        for (u16 i = 0; i < enemy_iter.count; ++i)
            delete_entity_id(world, enemy_iter.entities[i]);
    }

    // ToDo powerup
}

void delete_entity_name(ecs_world_t *world, const char *name)
{
    ecs_entity_t e = ecs_lookup(world, name);
    if (e > 0)
        ecs_delete(world, e);
    else
        TraceLog(LOG_DEBUG, "cannot find entity %s, delete aborted.", name);
}

void delete_entity_id(ecs_world_t *world, ecs_entity_t entity)
{
    if (entity > 0)
        ecs_delete(world, entity);
}
