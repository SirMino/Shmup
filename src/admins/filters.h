#ifndef FILTERS_ADMIN_H
#define FILTERS_ADMIN_H

#include "../../include/flecs/flecs.h"

typedef enum filters_type
{
    SHMUP_ECS_FILTER_FOLL_POS,
    SHMUP_ECS_FILTER_PLAY_POS_VEL,
    SHMUP_ECS_FILTER_ROLLINGBG,
    SHMUP_ECS_FILTER_BULLETS,
    SHMUP_ECS_FILTER_BB,
    SHMUP_ECS_FILTER_BB_NOTBULLETS,
    SHMUP_ECS_FILTER_ENEMY,
    SHMUP_ECS_FILTER_ENUM_END
} FiltersType;


void filters_admin_init(ecs_world_t *world);
void filters_admin_end();
ecs_filter_t *get_filter(int filter);

#endif