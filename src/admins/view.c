#include "../views/menu.h"
#include "../views/game.h"
#include "../views/game2.h"

#include "../../include/flecs/flecs.h"

#include "../globals.h"
#include "view.h"


static View _views[SHMUP_VIEW_ENUM_END];
static int _current_view;

void set_current_view(ecs_world_t *world, int view)
{
    if (view <= SHMUP_VIEW_ENUM_END)
    {
        _views[_current_view].end_void(world);
        _current_view = view;

        ecs_singleton_set(world, View,  { 
            _views[_current_view].init_void, 
            _views[_current_view].update_void,
            _views[_current_view].end_void,
            _views[_current_view].background_color });

        _views[_current_view].init_void(world);
    }
}

void view_admin_init(ecs_world_t *world)
{
    _current_view = 0;
    _views[SHMUP_VIEW_MENU] = (View) { menu_init, menu_update, menu_end, (Color) COLOR_PALETTE_1, NULL, false };
    _views[SHMUP_VIEW_GAME] = (View) { game_init, game_update, game_end, (Color) COLOR_PALETTE_2, NULL, true, { {120,160}, {256,1024}, 0.0, 1.0 } };
    _views[SHMUP_VIEW_GAME2] = (View) { game2_init, game2_update, game2_end, (Color) COLOR_PALETTE_2, NULL, false };

    ecs_singleton_set(world, View,  { 
        _views[_current_view].init_void, 
        _views[_current_view].update_void,
        _views[_current_view].end_void,
        _views[_current_view].background_color });
}

View * get_current_view()
{
    return & _views[_current_view];
}

void view_admin_end(ecs_world_t *world)
{
    _views[_current_view].end_void(world);
}