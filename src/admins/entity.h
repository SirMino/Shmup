#ifndef ENTITY_ADMIN_H
#define ENTITY_ADMIN_H

#include "../../include/flecs/flecs.h"
#include "../../include/raylib/raylib.h"

#include "../components/vectors.h"
#include "../components/enemy.h"
#include "texture.h"
#include "../enums/align.h"
#include "../structs/level.h"

void entity_admin_init(ecs_world_t *world);

void create_sprite(ecs_world_t * world, const char *name, TextureType sprite, Vector2 position);
void create_sprite2(ecs_world_t * world, const char *name, Texture * sprite, Vector2 position);
// void create_rolling_bg(ecs_world_t * world, const char *name, Texture * sprite, Vector2 position, Vector2 frame_start, Vector2 frame_end, level_background_t level_bg, bool repeat);
void create_rolling_bg(ecs_world_t * world, const char *name, level_background_t *level_bg, Vector2 position);
void create_text(ecs_world_t *world, const char *name, const char *text, u16 font_size, f32 pos_x, f32 pos_y, bool selected, bool highlight, enum Align h_align, enum Align v_align, Color color);
void create_menu_ui(ecs_world_t *world, char* menus[], u8 num, u8 start_y, void (* callback[])(ecs_world_t *world));
void delete_menu_ui(ecs_world_t *world, char* menus[], u8 num);
void create_player(ecs_world_t *world, const char *name, Vector2 position);
void spawn_enemy(ecs_world_t *world, EnemyType type, Vector2 position);
void spawn_explosion(ecs_world_t *world, Position pos);
void create_follower(ecs_world_t *world, const char *name, Position position);
void fire_bullet(ecs_world_t *world, Position position, f32 speed, Direction direction, u16 id);

void clear_level_entities(ecs_world_t *world);
void delete_entity_name(ecs_world_t *world, const char *name);
void delete_entity_id(ecs_world_t *world, ecs_entity_t entity);

#endif // ENTITY_HANDLER_H
