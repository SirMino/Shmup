#ifndef CONFIGURATION_ADMIN_H
#define CONFIGURATION_ADMIN_H

#include "../components/config.h"

void configuration_admin_init();
void configuration_admin_postraylib_init();
void configuration_admin_end();

game_config_t * get_configuration();

#endif
