#ifndef ENGINE_ADMIN_H
#define ENGINE_ADMIN_H

#include "../../include/flecs/flecs.h"

void engine_admin_init(ecs_world_t *world);
//void engine_handler_start(ecs_world_t *world, float delta_time);
//void engine_handler_update(ecs_world_t *world, float delta_time);
void engine_admin_end(ecs_world_t *world);

#endif