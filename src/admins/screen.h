#ifndef SCREEN_ADMIN_H
#define SCREEN_ADMIN_H

#include "../components/screen.h"

void set_current_screen(ecs_world_t *world, int screen);

void screen_admin_init(ecs_world_t *world);
//void screen_admin_set_title(ecs_world_t *world);
//void screen_admin_set_rain(ecs_world_t *world);

Screen * get_current_screen();

#endif