#include "level.h"

#include <stdlib.h>

#include "../../include/cjson/cJSON.h"

#include "files.h"
#include "texture.h"

static level_t levels[SHMUP_LEVELS_ENUM_END];
static u8 _current_level = 0;

static level_t _load_level(const char *level)
{
    //level_t *_level = malloc(sizeof * _level);
    level_t _level;
    char *level_file = load_text_file(level);
    cJSON *level_json = cJSON_Parse(level_file);
    level_background_t * backgrounds_array = NULL;
    u16 backgrounds_size = 0;

    if (level_json != NULL)
    {
        const cJSON *level_node = cJSON_GetObjectItemCaseSensitive(level_json, "level");
        const cJSON *level_name = cJSON_GetObjectItemCaseSensitive(level_node, "name");
        if (cJSON_IsString(level_name) && level_name->valuestring != NULL)
        {
            _level.name  = malloc(TextLength(level_name->valuestring) * sizeof(char));
            TextCopy(_level.name , level_name->valuestring);
        }
        cJSON *backgrounds_number = cJSON_GetObjectItemCaseSensitive(level_node, "backgrounds_number");
        _level.backgrounds_number = backgrounds_number->valueint;
        const cJSON *backgrounds = cJSON_GetObjectItemCaseSensitive(level_node, "backgrounds");
        cJSON *background= NULL;
        if (cJSON_IsArray(backgrounds))
        {
            cJSON_ArrayForEach(background, backgrounds)
            {
                cJSON *texture = cJSON_GetObjectItemCaseSensitive(background, "texture");
                cJSON *order = cJSON_GetObjectItemCaseSensitive(background, "order");
                cJSON *repeat = cJSON_GetObjectItemCaseSensitive(background, "repeat");
                cJSON *time = cJSON_GetObjectItemCaseSensitive(background, "time");
                cJSON *frame_start = cJSON_GetObjectItemCaseSensitive(background, "frame_start");
                cJSON *frame_end = cJSON_GetObjectItemCaseSensitive(background, "frame_end");

                backgrounds_array = realloc(backgrounds_array, sizeof(level_background_t) * (backgrounds_size + 1));
                backgrounds_array[backgrounds_size].texture = get_texture(texture->valueint); // TODO improvs
                backgrounds_array[backgrounds_size].order = order->valueint;
                backgrounds_array[backgrounds_size].repeat = repeat->valueint;
                backgrounds_array[backgrounds_size].time = time->valuedouble;
                if (frame_start != NULL && frame_end != NULL)
                {
                    Vector2 start = { cJSON_GetObjectItemCaseSensitive(frame_start, "x")->valuedouble, cJSON_GetObjectItemCaseSensitive(frame_start, "y")->valuedouble };
                    Vector2 end = { cJSON_GetObjectItemCaseSensitive(frame_end, "x")->valuedouble, cJSON_GetObjectItemCaseSensitive(frame_end, "y")->valuedouble };
                    backgrounds_array[backgrounds_size].frame_start = start;
                    backgrounds_array[backgrounds_size].frame_end = end;                    
                } else {
                    TraceLog(LOG_DEBUG, "level frame start and/or frame end are NULL");
                }
                backgrounds_size++;
            }
        }
        const cJSON *points = cJSON_GetObjectItemCaseSensitive(level_node, "points");
        cJSON *point= NULL;
        if (cJSON_IsArray(points))
        {
            cJSON_ArrayForEach(point, points)
            {
                cJSON *point_name = cJSON_GetObjectItemCaseSensitive(point, "name");
                cJSON *point_x = cJSON_GetObjectItemCaseSensitive(point, "x");
                cJSON *point_y = cJSON_GetObjectItemCaseSensitive(point, "y");

                if (TextIsEqual(point_name->valuestring, "spawn"))
                {
                    _level.spawn_point.x = point_x->valueint;
                    _level.spawn_point.y = point_y->valueint;
                } else if (TextIsEqual(point_name->valuestring, "halt"))
                {
                    _level.halt_point.x = point_x->valueint;
                    _level.halt_point.y = point_y->valueint;
                } else if (TextIsEqual(point_name->valuestring, "follower"))
                {
                    _level.follower_point.x = point_x->valueint;
                    _level.follower_point.y = point_y->valueint;
                }
            }
        }
    }

    cJSON_Delete(level_json);

    _level.backgrounds = malloc(sizeof(level_background_t) * backgrounds_size);
    for (u16 i = 0; i < backgrounds_size; ++i)
    {
        _level.backgrounds[i] = backgrounds_array[i];
    }

    free (backgrounds_array);
    free(level_file);

    return _level;
}

void levels_admin_init()
{
    levels[SHMUP_LEVELS_1] = _load_level("res/data/levels/level1.json");
    levels[SHMUP_LEVELS_2] = _load_level("res/data/levels/level2.json");
}

void levels_admin_end()
{
     for (u16 i = 0; i < SHMUP_LEVELS_ENUM_END; i++)
     {
        free(levels[i].name);
        free(levels[i].backgrounds);
     }
}

level_t *set_level(u8 level)
{
    if (level <= SHMUP_LEVELS_ENUM_END)
    {
        _current_level = level;
        return &levels[_current_level];
    }

    return NULL;
}

level_t *get_current_level()
{
    return &levels[_current_level];
}
