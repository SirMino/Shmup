#include "../engines/movement.h"
#include "../engines/render.h"
#include "../engines/view.h"
#include "../engines/text.h"
#include "../engines/input.h"
#include "../engines/player.h"
#include "../engines/enemy.h"
#include "../engines/bullet.h"
#include "../engines/audio.h"
#include "../engines/animation.h"
#include "../engines/collision.h"
#include "../engines/debug.h"

#include "engine.h"

void engine_admin_init(ecs_world_t *world)
{
    // init engines
    ECS_SYSTEM(world, input_engine_update, EcsOnUpdate, Input);
    // ECS_SYSTEM(world, follower_update, EcsOnUpdate, Follower, Position, Velocity, Speed);
    ECS_SYSTEM(world, follower_update, EcsOnUpdate, Follower, Position, TimeFlow);
    ECS_SYSTEM(world, rolling_bg_update, EcsOnUpdate, RollingBackground, Position, TimeFlow, Sprite);
    ECS_SYSTEM(world, movement_engine_update, EcsOnUpdate, [inout] Position, [in] Velocity);
    ECS_SYSTEM(world, player_engine_update, EcsOnUpdate, Player, Position, Velocity, Speed, Weapon, Animation);
    ECS_SYSTEM(world, enemy_engine_update, EcsOnUpdate, ShmEnemy, Position, Velocity, Speed);
    ECS_SYSTEM(world, bullet_engine_update, EcsOnUpdate, [inout] Bullet, Position, [inout] Velocity, [in] Direction, [in] Speed);
    ECS_SYSTEM(world, collision_engine_update, EcsOnUpdate, [inout] Bullet, [in] BoundingBox2D, [in] Position, [in] Velocity);
    ECS_SYSTEM(world, view_engine_update, EcsOnUpdate, View);
    ECS_SYSTEM(world, camera_update, EcsOnUpdate, Follower, Position);
    ECS_SYSTEM(world, render_view, EcsOnUpdate, View);
    ECS_SYSTEM(world, animation_engine_update, EcsOnUpdate, Sprite, Position, Animation);
    //ECS_SYSTEM(world, render_shadows, EcsOnUpdate, Sprite, Position);
    ECS_SYSTEM(world, render_sprites, EcsOnUpdate, Sprite, Position);
    ECS_SYSTEM(world, render_texts, EcsOnUpdate, RenderText, Position);
    ECS_SYSTEM(world, render_ui, EcsOnUpdate, UI, ButtonText, Position);
    ECS_SYSTEM(world, render_debug, EcsOnUpdate, [in] Input);
    ECS_SYSTEM(world, render_screen, EcsOnUpdate, GameScreen);
    ECS_SYSTEM(world, audio_engine_udpate, EcsOnUpdate, ShmSound);
}

void engine_admin_end(ecs_world_t *world)
{ }
