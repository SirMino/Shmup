#include "../screens/menu.h"
#include "../screens/screen1.h"
#include "../screens/screen2.h"

#include "../../include/flecs/flecs.h"

#include "../globals.h"
#include "screen.h"

#define SCREEN_NUMBER 3

static Screen _screens[SCREEN_NUMBER];
static int _current_screen;

void set_current_screen(ecs_world_t *world, int screen)
{
    if (screen <= SCREEN_NUMBER)
    {
        _screens[_current_screen].end_void(world);
        _current_screen = screen;

        ecs_singleton_set(world, Screen,  { 
            _screens[_current_screen].init_void, 
            _screens[_current_screen].update_void,
            _screens[_current_screen].render_void,
            _screens[_current_screen].end_void,
            _screens[_current_screen].background_color });

        _screens[_current_screen].init_void(world);
    }
}

void screen_admin_init(ecs_world_t *world)
{
    _current_screen = 0;
    _screens[0] = (Screen) { menu_screen_init, menu_screen_update, menu_screen_render, menu_screen_end, (Color) COLOR_PALETTE_1 };
    _screens[1] = (Screen) { screen1_screen_init, screen1_screen_update, screen1_screen_render, screen1_screen_end, (Color) COLOR_PALETTE_2 };
    _screens[2] = (Screen) { screen2_screen_init, screen2_screen_update, screen2_screen_render, screen2_screen_end, (Color) COLOR_PALETTE_3 };

    ecs_singleton_set(world, Screen,  { 
        _screens[_current_screen].init_void, 
        _screens[_current_screen].update_void,
        _screens[_current_screen].render_void,
        _screens[_current_screen].end_void,
        _screens[_current_screen].background_color });
}

Screen * get_current_screen()
{
    return & _screens[_current_screen];
}
