#include <stdlib.h>
#include "lua.h"

#include "../../include/lua/lua.h"
#include "../../include/lua/lualib.h"
#include "../../include/lua/lauxlib.h"

#include "configuration.h"

// Prototyping
static int _move_wrap();

static lua_State *L;

static char* _get_string(const char *field) {
    lua_getfield(L, -1, field);
    const char* value = lua_tostring(L, -1);
    lua_pop(L, 1);
    return (char*) value;
}

static u16 _get_int(const char *field) {
    lua_getfield(L, -1, field);
    u16 value = lua_tointeger(L, -1);
    lua_pop(L, 1);
    return value;
}

static u16 _get_color(const char* color_name, const char* color) {
    lua_pushstring(L, color_name);
    lua_gettable(L, -1);
    lua_pushstring(L, color);
    lua_gettable(L, -2);
    u16 val = lua_tointeger(L, -1);
    lua_pop(L, 2);

    return val;

}

void lua_admin_init() { 
    L = luaL_newstate();
    luaL_openlibs(L);
    // register methods
    lua_register(L, "move", _move_wrap);
}

static void _move(i16 x, i16 y, i16 time) {
    //todo
    TraceLog(LOG_DEBUG, "c linear move");
}

// wrapper func called by lua
static int _move_wrap() {
    TraceLog(LOG_DEBUG, "c move wrapper");
    i16 x = lua_tonumber(L, 1);
    i16 y = lua_tonumber(L, 2);
    i16 time = lua_tonumber(L, 3);

    _move(x, y, time);
    return 0;
}

void lua_admin_end() {
    lua_close(L);
}

game_config_t load_configuration() {

    game_config_t config = {0};
    lua_getglobal(L, "Configuration");
    if (lua_istable(L, -1)) {
        config.game_name = _get_string("name");
        config.fps = _get_int("fps");
        u8 palette_colors = _get_int("palette_colors");
        config.palette = malloc(sizeof(Color) * palette_colors);
        for (u8 i = 0; i < palette_colors; ++i) {
            config.palette[i].r = _get_color(TextFormat("col%d", i), "r");
            config.palette[i].g = _get_color(TextFormat("col%d", i), "g");
            config.palette[i].b = _get_color(TextFormat("col%d", i), "b");
        }
        //config.font = _get_string("font_name");
        config.font_base_size = _get_int("font_base_size");

    }

    return config;
}
