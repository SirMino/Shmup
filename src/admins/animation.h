#ifndef ANIMATION_ADMIN_H
#define ANIMATION_ADMIN_H

#include "../components/animation.h"

typedef enum animations
{
    SHMUP_ANIMATIONS_SHIP_IDLE,
    SHMUP_ANIMATIONS_SHIP_STRAFE,
    SHMUP_ANIMATIONS_ENEMY1,
    SHMUP_ANIMATIONS_EXPLOSIOND,
    SHMUP_ANIMATIONS_DRONE,
    SHMUP_ANIMATIONS_ENEMY2,
    SHMUP_ANIMATIONS_ENUM_END
} Animations;

void animations_admin_init();
void animations_admin_end();

Animation *get_animation(u16 animation);

#endif
