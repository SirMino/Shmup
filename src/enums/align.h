#ifndef ALIGN_ENUM_H
#define ALIGN_ENUM_H

enum Align
{
    NONE,
    LEFT,
    CENTER,
    RIGHT
};

#endif