#ifndef GLOBALS_H
#define GLOBALS_H

// https://lospec.com/palette-list/pola5
#define COLOR_PALETTE_1 {7,8,16}
#define COLOR_PALETTE_2 {24,40,74}
#define COLOR_PALETTE_3 {82,165,222}
#define COLOR_PALETTE_4 {172,214,246}
#define COLOR_PALETTE_5 {235,249,255}

#endif