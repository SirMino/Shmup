#include "admins/game.h"

int main(void)
{
    game_admin_init();
    game_admin_loop();
    game_admin_end();

    return 0;
}
