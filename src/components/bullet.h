#ifndef BULLET_COMPONENT_H
#define BULLET_COMPONENT_H

#include "../../include/flecs/flecs.h"
#include "../structs/datatypes.h"

typedef struct bullet
{
    u16 id;
    u16 texture;
    f32 lifespan;
    u16 damage;
} Bullet;

ECS_COMPONENT_DECLARE(Bullet);

#endif
