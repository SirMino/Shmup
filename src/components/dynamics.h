#ifndef VELOCITY_COMPONENT_H
#define VELOCITY_COMPONENT_H

#include "../../include/flecs/flecs.h"
#include "../../include/raylib/raylib.h"

#include "../structs/vectori2.h"

typedef struct velocity
{
    f32 x,y;
    Vector2 velocity;
    VectorI2 direction;
    u16 speed;
} Velocity;

ECS_COMPONENT_DECLARE(Velocity);

typedef struct direction
{
    u16 x, y;
} Direction;

ECS_COMPONENT_DECLARE(Direction);

typedef struct speed
{
    f32 speed;
} Speed;

ECS_COMPONENT_DECLARE(Speed);

#endif
