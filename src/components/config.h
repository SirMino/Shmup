#ifndef CONFIG_STRUCT_H
#define CONFIG_STRUCT_H

#include "../structs/datatypes.h"
#include "../../include/raylib/raylib.h"

typedef struct resolution
{
    int width;
    int height;
} game_resolution_t;

typedef struct configuration
{
    /* Game title bar*/
    char * game_name;
    /* FPS */
    u16 fps;
    /* Native resolution of the game */
    game_resolution_t native_resolution;
    /* Resolution to render the game */
    game_resolution_t render_resolution;
    /* Font to load */
    char * font_name;
    /* Font base size */
    u8 font_base_size;
    /* Font filter to apply */
    u8 font_filter;
    /* Font for game text*/
    Font font;
    /* Palette (color array) of the game */
    Color * palette;

    bool running;
    
    // Custom properties
    u16 custom_prop1;
} game_config_t;

#endif
