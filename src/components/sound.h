#ifndef SOUND_COMPONENT_H
#define SOUND_COMPONENT_H

#include "../structs/datatypes.h"
#include "../admins/audio.h"

#include "../../include/flecs/flecs.h"
//#include "../../include/raylib/raylib.h"

typedef struct sound
{
    SFXType type;
    bool played;
    f32 pan;
} ShmSound;

ECS_COMPONENT_DECLARE(ShmSound);

#endif
