#ifndef VECTORS_COMPONENT_H
#define VECTORS_COMPONENT_H

#include "../../include/flecs/flecs.h"
#include "../../include/raylib/raylib.h"

typedef Vector2 Position;
typedef Vector2 Direction;
typedef Vector2 Velocity;

ECS_COMPONENT_DECLARE(Position);
ECS_COMPONENT_DECLARE(Direction);
ECS_COMPONENT_DECLARE(Velocity);

#endif