#ifndef ANIMATION_COMPONENT_H
#define ANIMATION_COMPONENT_H

#include "../structs/datatypes.h"

#include "../../include/flecs/flecs.h"
#include "../../include/raylib/raylib.h"

typedef struct animation_frame
{
    Vector2 coords;
    Vector2 size;
    //f32    elapsed_time;
    u16 sprite_index;
} AnimationFrame;

typedef struct animation
{
    f32 animation_speed;
    f32 frame_timer;
    u16 current_frame;
    // char *current_state; // enum
    u16 frames_number;
    AnimationFrame *frames;
    bool flip_x;
    u16 id;
    bool loop;
    bool destroy_after_run;
} Animation;

// ECS_COMPONENT_DECLARE(AnimationFrame);
ECS_COMPONENT_DECLARE(Animation);

#endif
