#ifndef PLAYER_COMPONENT_H
#define PLAYER_COMPONENT_H

#include "../../include/flecs/flecs.h"
#include "../../include/raylib/raylib.h"
#include "../structs/datatypes.h"

//#include "weapon.h"

typedef struct player
{
    //Camera2D camera;
    u16 shields;
    u16 life;
    bool input_active;
    //Weapon *weapon;

} Player;

ECS_COMPONENT_DECLARE(Player);

#endif
