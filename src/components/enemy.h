#ifndef ENEMY_COMPONENT_H
#define ENEMY_COMPONENT_H

#include "../structs/datatypes.h"
#include "../../include/flecs/flecs.h"

typedef enum enemy_type {
    SHMUP_ENEMY_POPCORN1,
    SHMUP_ENEMY_POPCORN2
} EnemyType;

typedef struct enemy {
    EnemyType type;
    f32 init_wait;
    f32 shoot_cooldown;
} ShmEnemy;

ECS_COMPONENT_DECLARE(ShmEnemy);

#endif
