#ifndef UI_COMPONENT_H
#define UI_COMPONENT_H

#include "../enums/align.h"

#include "../../include/flecs/flecs.h"
#include "../../include/raylib/raylib.h"

// typedef void (* callback_void_t)(ecs_world_t *world);

typedef struct buttontext
{
    const char *text;
    //int font_size;
    //bool selected;
    //bool highlight;
    //enum Align h_align;
    //enum Align v_align;
    //Color color;
    void (* callback)(ecs_world_t *world);
} ButtonText;

ECS_COMPONENT_DECLARE(ButtonText);

#endif