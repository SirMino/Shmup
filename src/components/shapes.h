#ifndef SHAPES_COMPONENTS_H
#define SHAPES_COMPONENTS_H

#include "../../include/flecs/flecs.h"
#include "../../include/raylib/raylib.h"

typedef Rectangle BoundingBox2D;

ECS_COMPONENT_DECLARE(BoundingBox2D);

#endif