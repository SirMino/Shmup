#ifndef WEAPON_COMPONENT_H
#define WEAPON_COMPONENT_H

#include "../../include/flecs/flecs.h"
#include "../../include/raylib/raylib.h"

#include "../structs/vectori2.h"

typedef struct weapon
{
    u16 id;
    char *name;
    f32 fire_interval;
    i8 bullet_id;
    f32 speed;
    f32 damage_multiplier;
    i8 noozles_number;
    VectorI2 *noozles;
    // VectorI2 fire_offset;
    //Texture *bullet;
} Weapon;

ECS_COMPONENT_DECLARE(Weapon);

#endif
