#ifndef SCREEN_COMPONENT_H
#define SCREEN_COMPONENT_H

#include "../../include/flecs/flecs.h"
#include "../../include/raylib/raylib.h"

typedef struct screen
{
    Rectangle source;
    Rectangle destination;

} GameScreen;

ECS_COMPONENT_DECLARE(GameScreen);

#endif