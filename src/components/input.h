#ifndef INPUT_COMPONENT_H
#define INPUT_COMPONENT_H

#include "../../include/flecs/flecs.h"

typedef struct input
{
    bool esc;
    bool debug;
    bool up;
    bool down;
    bool left;
    bool right;
    bool z;
    bool x;
    bool c;
    bool enter;
    int8_t x_axis, y_axis;
} Input;

ECS_COMPONENT_DECLARE(Input);

#endif