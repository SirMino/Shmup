#ifndef SPEED_COMPONENT_H
#define SPEED_COMPONENT_H

#include "../../include/flecs/flecs.h"
#include "../structs/datatypes.h"

typedef struct speed
{
    f32 min_value;
    f32 max_value;
} Speed;

ECS_COMPONENT_DECLARE(Speed);

typedef struct timeflow
{
    f32 time_elapsed;
    f32 time_total;
} TimeFlow;

ECS_COMPONENT_DECLARE(TimeFlow);

#endif
