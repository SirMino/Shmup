#ifndef PARTICLE_COMPONENT_H
#define PARTICLE_COMPONENT_H

#include "../../include/flecs/flecs.h"
#include "../../include/raylib/raylib.h"
#include "../structs/datatypes.h"

typedef struct particle
{
    f32 lifespan;
    //Color color;
    f32 alpha;
    f32 size;
    //ParticleType type;
    u16 emitter;
    bool active;
} Particle;

ECS_COMPONENT_DECLARE(Particle);

typedef struct particle_emitter
{
    u16 particles_number;
    f32 particle_life;
    bool loop;
    bool running;
    Vector2 origin;
    Vector2 origin_size;
    f32 timer;
    
    /* emitter type */
    // ParticleEmitterType type;
    u16 trail_parent;

    /* seconds between one particle generation and the next */
    f32 spawn_time;

    u16 scale;
    f32 scale_random_ratio; /* mgari usare il metodo module https://www.cplusplus.com/reference/cstdlib/rand/ */

} ParticleEmitter;

ECS_COMPONENT_DECLARE(ParticleEmitter);

#endif
