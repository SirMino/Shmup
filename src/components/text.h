#ifndef TEXT_COMPONENT_H
#define TEXT_COMPONENT_H

#include "../enums/align.h"

#include "../../include/flecs/flecs.h"
#include "../../include/raylib/raylib.h"

typedef struct text
{
    const char * text;
    int font_size;
    bool selected;
    bool highlight;
    enum Align h_align;
    enum Align v_align;
    Color color;
} RenderText;

ECS_COMPONENT_DECLARE(RenderText);

#endif