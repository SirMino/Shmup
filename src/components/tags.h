#ifndef TAGS_H
#define TAGS_H

#include "../../include/flecs/flecs.h"

ECS_DECLARE(Follower);
ECS_DECLARE(UI);
ECS_DECLARE(RollingBg);
ECS_DECLARE(Enemy);
ECS_DECLARE(PlayerTag);

#endif
