#ifndef BEHAVIOUR_COMPONENT_H
#define BEHAVIOUR_COMPONENT_H

#include "../components/vectors.h"
#include "../components/speed.h"

struct package {
    Position *position;
    Velocity *velocity;
    Speed *speed;
    f32 duration;
    f32 timer;
    f32 delta_time;
};

#endif
