#ifndef STATE_COMPONENT_H
#define STATE_COMPONENT_H

#include "../../include/flecs/flecs.h"

typedef struct state
{
    bool rain;
    bool debug;
} State;

ECS_COMPONENT_DECLARE(State);

#endif