#ifndef VIEW_COMPONENT_H
#define VIEW_COMPONENT_H

#include "../../include/flecs/flecs.h"
#include "../../include/raylib/raylib.h"

typedef void (* init)(ecs_world_t *world);
typedef void (* update)(ecs_iter_t *it);
typedef void (* end)(ecs_world_t *world);

typedef struct view
{
    init init_void;
    update update_void;
    end end_void;
    Color background_color;
    Texture2D * background;
    bool has_camera;
    Camera2D camera;
} View;

ECS_COMPONENT_DECLARE(View);

#endif