#ifndef SPRITE_COMPONENT_H
#define SPRITE_COMPONENT_H

#include "../../include/flecs/flecs.h"
#include "../../include/raylib/raylib.h"

#include "../structs/vectori2.h"

typedef struct sprite
{
    Texture2D * texture;
    Rectangle frame;
    VectorI2 offset;
    bool has_shadow;
} Sprite;

ECS_COMPONENT_DECLARE(Sprite);

typedef struct rolling_background
{
    Vector2 frame_start;
    Vector2 frame_end;
} RollingBackground;

ECS_COMPONENT_DECLARE(RollingBackground);

#endif